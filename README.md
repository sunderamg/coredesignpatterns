Advanced Java Project that has the following code
1. Core GOF Design Patterns examples (taken from Head First DP book)
2. Generic Java programs
3. Concurrent package examples

This would help someone trying to get a hang of above concepts

Java 6 +
IDE Such as Eclipse, IDEA or NetBeans

is all that is required to download the code and run the examples. Rest all is self documenting.