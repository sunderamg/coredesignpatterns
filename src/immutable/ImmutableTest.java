package immutable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Aug 5, 2012
 */
public class ImmutableTest {
  public static void main(String[] args) throws ClassNotFoundException,
          NoSuchFieldException, IllegalAccessException, InstantiationException,
          NoSuchMethodException, InvocationTargetException {
    OneValueCache cache = new OneValueCache(6, new int[] {1,2,3,6});
    int [] factors = cache.getFactors(6);
    stdout(cache.toString() + " Returned reference: " + factors);

    Person p = new Person(1);
    Class<?> personClass = Class.forName("immutable.Person");
//    Person person = (Person) personClass.getDeclaredConstructor(String.class).newInstance("gsun");
    Field field = personClass.getDeclaredField("rank");
    field.setAccessible(true);
    field.set(p, 3);
    stdout("person is " + p.getRank());
  }
}
