package immutable;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Oct 11, 2013
 *
 * Demonstrates use of Immutable Object such as OVC in conjunction with volatile. It's simple, yet
 * a powerful alternative over locking as it's equally efficient for moderate contention levels
 */
public class Factors {
  private volatile OneValueCache cache; //Immutable object

  public int[] getFactors(int t) {
    int [] factors = cache.getFactors(t); //Because volatile ensures read visibility
																					//no locks are needed
    if (factors == null) {
        factors = factor(t);
        stdout("Adding to cache");
        cache = new OneValueCache(t, factors);
    }

    return factors;
  }

  private int[] factor(int t) {
    return new int[0];
  }
}
