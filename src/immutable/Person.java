package immutable;

/**
 * User: gsunderam
 * Date: Apr 21, 2014
 */
public final class Person {
  private final int rank;

  public Person(int name) {
    this.rank = name;
  }

  public Person getPerson() {
    return new Person(this.rank);
  }

  public int getRank() {
    return rank;
  }
}
