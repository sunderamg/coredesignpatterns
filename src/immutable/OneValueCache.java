package immutable;

import java.util.Arrays;

/**
 * User: gsunderam
 * Date: Aug 5, 2012
 *
 * One way to make the class immutable. Mark all states final, properly construct objects from outside
 * and set state fields in the constructor.
 * Arrays.copyOf is needed as shown below to make the class immutable. The reason is that the contents of the
 * lastFactors can be altered by clients. clone() will also work 
 */
public class OneValueCache {
    private int lastNumber;
    private final int[] lastFactors;

    public OneValueCache(int i,
                         int[] factors) {
        lastNumber = i;
        lastFactors = Arrays.copyOf(factors, factors.length);
    }

  /** No locks needed as this is immutable */  
	public int[] getFactors(int i) {
        if (lastNumber == 0 || !(lastNumber == i))
            return null;
        else
            return Arrays.copyOf(lastFactors, lastFactors.length);
    }

    public String toString() {
      return "Factors of " + lastNumber + " are " + Arrays.toString(lastFactors);
    }
}
