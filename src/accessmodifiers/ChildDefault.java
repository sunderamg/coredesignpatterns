package accessmodifiers;

import corejava.ParentDefault;
import log.Logger;

/**
 * User: gsunderam
 * Date: Jan 15, 2011
 */
public class ChildDefault extends ParentDefault {
    public ChildDefault(String s) {
        super(s);
    }

    public static void main(String[] args) {
        ChildDefault c = new ChildDefault("test");
        c.accessSubclass();
    }

    private void accessSubclass() {
        ParentDefault p = new ParentDefault("test");
        //Logger.stdout(p.testVar); //Can't access testVar via Reference of super class as its protected
        Logger.stdout(testVar); //can ONLY inherit testVar as its protected and protected IS ONLY for kids!
    }
}
