package accessmodifiers;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Mar 14, 2011
 */
public class ChildSubclass extends ChildOfParent2 {

    public ChildSubclass(String s) {
        super(s);
    }

    public void accessProtectedVar() {
        stdout("protected variable from a subclass " + testVar); //Access by inheritance using reference of "this" - OK
    }

    public static void main(String[] args) {
        ChildOfParent2 csub = new ChildSubclass("test");
        ChildSubclass child = new ChildSubclass("cat");
        child.accessProtectedVar();
        stdout(csub.testVar); //Access by inheritance using reference of subclass - OK
        csub.setCountry("New America");   //set the country here
        stdout("subclass country " + csub.getCountry()); // prints New America. Polymorphism
    }
}
