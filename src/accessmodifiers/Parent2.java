package accessmodifiers;

import log.Logger;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Jan 15, 2011
 * Time: 5:30:42 PM
 * To change this template use File | Settings | File Templates.
 */
class Parent2 {
    protected int testVar = 3;

     Parent2(String s) {
        stdout("Called super class 1-arg " + s);

    }

    public void addVar(int o) {
        stdout("Super add var primitive " + o);
    }

    //Parent2() {}
}
