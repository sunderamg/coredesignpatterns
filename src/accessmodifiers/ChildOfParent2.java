package accessmodifiers;

import corejava.ParentDefault;
import log.Logger;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Jan 15, 2011
 * Time: 5:14:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChildOfParent2 extends Parent2 {

    private static String name = "GS";
    String country;

    ChildOfParent2(String s) {
        super(s);
        this.country = s;
    }

    /**
     * Note that if we have this() call below the code compiles and runs fine.
     * comment the this() below, then the code will not compile.
     */
    ChildOfParent2() {
       this(name);  //can access ONLY static members here
    }
    
    public static void main(String[] args) {
        ChildOfParent2 c = new ChildOfParent2("");
       // c.accessSubclass();
        //stdout(c.testVar);   //This access is by inheritance and so always possible!
        c.addVar(new Integer(3));  //Only wrapped arg calls the sub class, otherwise super version is called
        Parent2 p = new ChildOfParent2();
        p.addVar(5);   //class super version as ref type determines which overloaded method is called
    }

    private void accessSubclass() {
        ParentDefault p = new ParentDefault("");
        //Logger.stdout(p.testVar);  // No reference access
        Logger.stdout(testVar);
    }

    /**
     * This changes the argument type to WRAPPER object. NOTE THAT THIS IS AN OVERLOAD, NOT AN OVERRIDE
     *
     * @param o
     */
    public void addVar(Integer o) {
       stdout("subclass Boxed wrapper " + o); 
    }

    public String getCountry() {
       return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
