package command;

/**
 * Layer 2.
 */
public abstract interface Command {
    public abstract void execute();
}
