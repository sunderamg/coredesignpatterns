package command;

import static log.Logger.stdout;

/**
 * Layer 3 - HERE IS THE ACTUAL IMPLEMENTATION FO BUSINESS LOGIC
 */
class Light {
    public void turnOn( ) {
         stdout("command.Light is on ");
    }
    
    public void turnOff( ) {
         stdout("command.Light is off");
    }
}

