package command;

/**
 * This class is the orchestrator of the show. Has no knowledge about fan or light.
 * Those reside in the upCommand or downCommand classes. Layer 1
 */
class Switch {
    
    private Command upCommand;
    private Command downCommand;
    
    public Switch(Command Up, Command Down) {
       upCommand = Up; // concrete command.Command registers itself with the invoker
       downCommand = Down;
    }

    void flipUp() { // invoker calls back concrete command.Command, which executes the command.Command on the receiver
       upCommand.execute() ;
    }

    void flipDown() {
       downCommand.execute();
    }
}

