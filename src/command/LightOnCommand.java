package command;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Oct 18, 2010
 * Time: 4:37:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class LightOnCommand implements Command {

    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.turnOn();
    }
}
