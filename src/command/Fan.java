package command;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Oct 18, 2010
 * Time: 4:35:25 PM
 * To change this template use File | Settings | File Templates.
 */
class Fan {
    public void startRotate() {
       stdout("command.Fan is rotating");
    }
    public void stopRotate() {
       stdout("command.Fan is not rotating");
    }
}

