package command;

/**
 * Fan stop command
 */
public class FanStopCommand  implements Command {
    private Fan fan;

    public FanStopCommand(Fan fan) {
        this.fan = fan;
    }
    
    public void execute() {
        fan.stopRotate();
    }
}
