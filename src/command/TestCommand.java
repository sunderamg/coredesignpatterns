package command;

/**
 * Switch is the orchestrator of the show. It created the objects, commands and wires them!
 * It does NOT know about light or fan. That's the responsibility of the Light and Fan Commands
 */
public class TestCommand {

    public static void main(String [] args) {
        Light light = new Light();
        //Good use of dependency injection. inject light in to these commands
        LightOnCommand lightOnCommand = new LightOnCommand(light);
        LightOffCommand lightOffCommand = new LightOffCommand(light);

        //command.Switch needn't know about HOW to perform the operation, but it just needs to know to issue the command
        Switch s = new Switch(lightOnCommand, lightOffCommand); //command.Command patterns are lightOnCommand and off commands
        s.flipUp(); //Inject commands in to Switch as above
        s.flipDown();
        //Fan code
        Fan fan = new Fan();
        //Inject commands in to switch and fan into the FanCommands. This is REALLY dependency injection
        Switch fanSwitch = new Switch(new FanStartCommand(fan), new FanStopCommand(fan));
        fanSwitch.flipUp();
        fanSwitch.flipDown();
    }

}
