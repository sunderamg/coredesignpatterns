package threadTests;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 31, 2010
 * Time: 8:00:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class InSync extends Thread {

    private StringBuffer letter;
    private String name;

    public InSync(StringBuffer letter, String name) {
        super(name);
        this.letter = letter;
        this.name = name;
    }

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("A");
        Thread a = new InSync(sb, "A");
        Thread b = new InSync(sb, "B");
        Thread c = new InSync(sb, "C");

        a.start();b.start();c.start();
        try {
            a.join();b.join();c.join(); //Makes the main thread wait for these threads to finish
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        stdout("Thread " + Thread.currentThread().getName() + " completed");
    }

    public void run() {
        synchronized (letter) {
            for (int i = 0;i < 100;i++)
                stdout(letter.toString());
            stdout("Completed " + letter.charAt(0));
            stdout("------------------------------------");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            char ch = letter.charAt(0);
            letter.setCharAt(0, ++ch); // letter.setCharAt(0, (char)(ch+1)); will work too
                                       //int to char cast narrowing conversion so explicit cast required
                                        //ch++
        }
        stdout("letter thread " + Thread.currentThread().getName() + " completed");

    }
}
