package threadTests;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Jan 1, 2011
 */
public class Reader implements Runnable {
    final Calculator calc;

    public Reader(Calculator calc) {
        this.calc = calc;
    }
    public void run() {
        synchronized (calc) { //Always lock on a final object to have the intended semantics always
            try {
                if (calc.sum == 0) { //Not doing this may cause a thread to wait for ever 
                    stdout("Waiting for the results");
                    calc.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stdout("The total for " + Thread.currentThread().getName() + " is " + calc.sum);
        }
    }

    public static void main(String[] args) {
        Calculator c = new Calculator(10);
        Thread t1 = new Thread(new Reader(c), "Reader A");
        Thread t2 = new Thread(new Reader(c), "Reader B");
        Thread t3 = new Thread(new Reader(c), "Reader C");
        t1.start();
        t2.start();
        t3.start();
        Thread cal = new Thread(c);
        cal.start();
    }
}

class Calculator implements Runnable {
    int sum;
    final int n;

    public Calculator(int n) {
        this.n = n;
    }

    public synchronized void run() {
        for (int i = 0;i < n;i++)
            sum = n * (n + 1) / 2;
        notifyAll();
    }
}