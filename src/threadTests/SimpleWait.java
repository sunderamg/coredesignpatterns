package threadTests;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Jan 1, 2011
 * Time: 4:14:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleWait {

    public static void main(String[] args) {
        NotifyingThread threadA = new NotifyingThread();
        threadA.setName("A");
        threadA.start();
        NotifyingThread threadB = new NotifyingThread();
        threadB.setName("B");
        threadB.start();

        synchronized (threadB) {
            stdout("waiting for notification B...");
            try {
                threadB.wait();
                stdout("Notification received B");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stdout("Total is " + threadB.total);
        }
        synchronized (threadA) {
            try {
              //  if (threadA.total == 0) {  //ALWAYS check on the condition for which wait is desired.
                    stdout("waiting for notification A");
                    threadA.wait();    //Possibility of A being notified before wait on threadA is invoked
                    stdout("Notification received A");
                //} else {
                  //  stdout("Notification received before waiting A"); //No guaranteed behavior
                //}
            } catch (InterruptedException e) {
                e.printStackTrace(); 
            }
            stdout("Total is " + threadA.total);
        }
        
    }
}

class NotifyingThread extends Thread {
    int total;

    public synchronized void run() {
        for (int i = 0;i < 100;i++)
           total += i;
        notifyAll();
        stdout("Notification sent for thread " + Thread.currentThread().getName());
    }

}
