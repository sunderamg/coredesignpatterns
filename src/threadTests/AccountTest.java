package threadTests;

import log.Logger;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 31, 2010
 * Time: 12:41:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class AccountTest implements Runnable {
    private Account acct = new Account();

    public static void main(String[] args) {
        AccountTest test = new AccountTest();
        Thread one = new Thread(test);
        Thread two = new Thread(test);
        one.setName("Fred");
        two.setName("Lucy");

        one.start();
        two.start();
    }


    public void run() {
        for (int i = 0;i < 10;i++) {
            makeWithDrawal(10);
            if (acct.getBalance() < 0) {
                stdout("Account is overdrawn");
            }
        }

    }

    /**
     * This is an example of check-then-act. BOTH the operations getBalance AND withdraw on the acct object
     * need to be synchronized in an atomic unitof work for the threads to report the balance correctly
     * 
     * @param amt
     */
    private synchronized void makeWithDrawal(int amt) {
        if (acct.getBalance() >= amt) {  //Check
            stdout("Thread " + Thread.currentThread().getName() + " is going to withdraw");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            acct.withdraw(amt); //Act. Refer Brian Goetz "Java Concurrency" in practice. Check-then-act
                                // MUST happen atomically
         }  else {
            stdout("Not enough to withdraw for " + Thread.currentThread().getName() + " balance " +
                    acct.getBalance());
        }
    }
}

class Account {
    private int balance = 50;

    public void withdraw(int amt) {
        balance -= amt;
    }

    public int getBalance() {
        stdout("balance remaining " + balance);
        return balance;
    }
}