package threadTests;

/**
 * User: gsunderam
 * Date: Oct 20, 2010
 */
public class BasicThreadTests {
    public static void main(String [] args) throws InterruptedException {
         Thread t = new Thread();
         t.start();
         System.out.println("X");
         synchronized (t) {
            t.wait(10000);
         }
         System.out.println("Y");
    }
}
