package EnumTypes;

import static log.Logger.stdout;

/**
 * Date: Dec 3, 2010
 * Illustrate enum data typed added in java 5
 */
public class CoffeeEnum {
    //Inline declaration within a class definition
    enum CoffeeSize {BIG, SMALL, HUGE};  //Just a template

    CoffeeSize size;  //create a reference to hold the enum type

    public static void main(String[] args) {
          CoffeeEnum coffee = new CoffeeEnum();
          coffee.size = CoffeeSize.BIG;
          stdout(CoffeeSize.BIG);  //can reference an enum directly lie this
    }
}
