package EnumTypes;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Mar 21, 2011
 * Time: 10:49:01 PM
 * To change this template use File | Settings | File Templates.
 */
enum Country {INDIA, CHINA, JAPAN, UK}

public class CountryEnum {
    public static void main(String[] args) {
        Country c = Country.CHINA;
         switch(c) {
            case INDIA: stdout("India is great");
            case CHINA: {
                stdout("China is great " + c.ordinal());
            }
            case JAPAN: stdout("Japan is great");
            default: stdout("UK is great");
        }
    }
}
