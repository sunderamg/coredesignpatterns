package EnumTypes;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Apr 5, 2011
 * Illustration of the type safe enum pattern
 */
public class CoinVisitor {

    public static void main(String[] args) {
        CoinVisitor cv = new CoinVisitor();
        Coin c = Coin.PENNY;
        c.accept(cv);
        c = Coin.DIME;
        c.accept(cv);
        c = Coin.NICKEL;
        c.accept(cv);
        c = Coin.QUARTER;
        c.accept(cv);
    }

     void visitPenny(Coin c) {
         stdout("Coin visited is " + c);
     }

     void visitNickel(Coin c) {
        stdout("Coin visited is " + c);
     }

     void visitDime(Coin c) {
        stdout("Coin visited is " + c);
     }

     void visitQuarter(Coin c) {
        stdout("Coin visited is " + c); 
     }
 }

enum Coin {
     PENNY {
       void accept(CoinVisitor cv) {
           cv.visitPenny(this);
       }
     },
     NICKEL {
       void accept(CoinVisitor cv) {
         cv.visitNickel(this);
       }
     },
     DIME {
       void accept(CoinVisitor cv) {
         cv.visitDime(this);
       }
     },
     QUARTER {
       void accept(CoinVisitor cv) {
         cv.visitQuarter(this);
       }
     };
    
     abstract void accept(CoinVisitor cv);

}
