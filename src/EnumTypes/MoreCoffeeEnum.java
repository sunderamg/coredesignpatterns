package EnumTypes;

import java.util.HashMap;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 3, 2010
 * Time: 3:08:58 PM
 * Advanced enum type discussion
 */
public class MoreCoffeeEnum {
    CoffeeSize coffeeSize;

    public static void main(String[] args) {
        MoreCoffeeEnum coffee1 = new MoreCoffeeEnum();
        coffee1.coffeeSize = CoffeeSize.SMALL;
        MoreCoffeeEnum coffee2 = new MoreCoffeeEnum();
        coffee2.coffeeSize = CoffeeSize.BIG;
        MoreCoffeeEnum coffee3 = new MoreCoffeeEnum();
        coffee3.coffeeSize = CoffeeSize.HUGE;
        stdout("Coffee 1 ounces: " + coffee1.coffeeSize.getOunces() + " Lid Size: " + coffee1.coffeeSize.getLidCode());
        stdout("Coffee 2 ounces: " + coffee2.coffeeSize.getOunces() + " Lid Size: " + coffee2.coffeeSize.getLidCode());
        stdout("Coffee 3 ounces: " + coffee3.coffeeSize.getOunces() + " Lid Size: " + coffee3.coffeeSize.getLidCode());
        //stdout("Static coffee size: " + CoffeeSize.getOunces());
        stdout("Enum map");
        coffee1.coffeeSize = CoffeeSize.getEnum("Big");
        stdout("Coffee size via enum " + coffee1.coffeeSize.getLidCode() + " capacity " + coffee1.coffeeSize.getOunces());
    }


}

enum CoffeeSize { 
    SMALL(8, "Small"), BIG(10, "Big"), HUGE(16, "Huge") {
        // Lid code specific for HUGE
        public String getLidCode() {   //Block of code specific to HUGE: "constant specific class body"
            return "A";
        }
    };

    private static HashMap<String, CoffeeSize> coffeeEnum = new HashMap<String, CoffeeSize>();
    private String description;
    private int ounces;

    public int getOunces() {
        return ounces;
    }

    static {
        for (CoffeeSize coffeeSize : values()) {
            stdout("populating enum map ");
            coffeeEnum.put(coffeeSize.description, coffeeSize);
        }
    }

    CoffeeSize(int ounces, String description) {
        stdout("Enum constructor invoked for " + ounces);
        this.ounces = ounces;
        this.description = description;
    }

    /**
     * Default lid code for all sizes
     * @return
     */
    public String getLidCode() {
        return "B";
    }

    public static CoffeeSize getEnum(String description) {
        return coffeeEnum.get(description);
    }
}
