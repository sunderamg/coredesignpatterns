package polymorphism;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 9, 2010
 * Time: 12:00:43 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Node {
      public abstract double evaluate();
}
