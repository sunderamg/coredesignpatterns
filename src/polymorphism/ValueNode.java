package polymorphism;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 9, 2010
 * Time: 12:11:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValueNode extends Node {

    double value;
    @Override
    public double evaluate() {
       return value;
    }
}
