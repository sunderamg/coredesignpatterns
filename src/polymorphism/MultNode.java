package polymorphism;

/**
 * User: gsunderam
 * Date: Nov 9, 2010
 * Time: 12:14:52 PM
 * A node for multiplication.
 */
public class MultNode extends OpNode {
    @Override
    public double evaluate() {
        return left.evaluate() * right.evaluate();
    }
}
