package polymorphism;

import static log.Logger.*;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 13, 2010
 * Time: 10:12:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class UseAnimals {
    public static void main(String [] args) {
        UseAnimals ua = new UseAnimals();
        Animal a; // = new Animal();
        //Horse horse = (Horse) a; //Error here. ClassCastException as a refers to Animal object and NOT Horse
        Horse h = new Horse("temp");
        a = h;
        ua.doStuff(a); //Animal version is invoked. ref type determines which overloaded method is called
        ua.doStuff(h);
        
    }

    /**
     * These two are overloaded methods
     * @param a
     */
    public void doStuff(Animal a) {
        stdout("Animal version");
    }

    public void doStuff(Horse a) {
        stdout("Horse version");
    }
}

class Animal {
    Animal(){
        super(); //compiler inserts this here
        stdout("Super Default");  //can be Called by the runtime from any constructor in the subclass
    }
    Animal(String s) {
        this();
        stdout("Super overloaded");
    }
}
class Horse extends Animal {
    Horse(String s) {
        super(s); //Minus this the super default is invoked as the compiler adds super() in the beginning of the constructor
        stdout("Sub");
    }
}