package polymorphism;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 9, 2010
 * Time: 12:02:43 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class OpNode extends Node {

    Node left;
    Node right;

    @Override
    public abstract double evaluate();
}
