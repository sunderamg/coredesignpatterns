package polymorphism;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 11, 2010
 * Time: 7:38:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Parent {
    String name;

    public Parent() {
        System.out.println("Parent constructor");
        this.name = "Mary Galloway";
    }

    public String getDisplay() {
        return "Parent's name is " + getName();
    }

    public void setDisplay() {
          System.out.println(this.getDisplay());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Child extends Parent {
    Child() {
        System.out.println("child constructor");
        setName("Mike");
    }

    public String getDisplay() {
        return "Child's name is " + getName();
    }

    public static void main(String [] args) {
         //Child child = new Child();
        Parent parent = new Child();
        parent.setDisplay();
    }
}
