package polymorphism;

import static log.Logger.stdout;

/**
 * The goal of this package is to Model the operation below as a tree using objects: states and operations
 * states are numbers like 1, 2 or 3. Operations are Add and multiply. Refer to the individual classes in this
 * class to see how its implemented. Node is "SUPERCLASS". ValueNode, AddNode and MultNode are other main participants 
 */
public class TestPloy {

    public static void main(String [] args) {
        //1 + 2 * 3
        if (args != null && args.length != 3) {
            usage();
            System.exit(0);
        }
        OpNode mult = new MultNode();
        ValueNode value = new ValueNode();
        value.value = Double.parseDouble(args[0]);
        ValueNode value2 = new ValueNode();
        value2.value = Double.parseDouble(args[1]);
        mult.left = value;
        mult.right = value2;
        OpNode add = new AddNode();
        ValueNode val = new ValueNode();
        val.value = Double.parseDouble(args[2]);
        add.left = val;
        ValueNode val2 = new ValueNode();
        val2.value = mult.evaluate();
        add.right = val2;
        double finalValue = add.evaluate();
        stdout(finalValue);
    }

    private static void usage() {
        stdout("java TestPloy 2 3 1");
    }
}
