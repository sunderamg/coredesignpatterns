package polymorphism;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 9, 2010
 * Time: 12:13:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddNode extends OpNode {
    @Override
    public double evaluate() {
        return left.evaluate() + right.evaluate();
    }
}
