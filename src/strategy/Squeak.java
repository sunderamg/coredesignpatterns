package strategy;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: May 14, 2011
 * Time: 10:39:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class Squeak implements QuackBehavior {

  public void quack() {
    stdout("This is squeaking duck");
  }
}
