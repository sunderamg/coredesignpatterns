package strategy;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: May 14, 2011
 * Time: 10:41:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class MuteQuack implements QuackBehavior {

  public void quack() {
    stdout("This duck is muted");
  }
}
