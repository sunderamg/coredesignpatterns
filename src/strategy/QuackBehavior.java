package strategy;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: May 14, 2011
 * Time: 10:28:34 AM
 * To change this template use File | Settings | File Templates.
 */
public interface QuackBehavior {
  public void quack();
}
