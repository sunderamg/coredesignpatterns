package strategy;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: May 14, 2011
 * Time: 10:37:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class FlyWithRocket implements FlyBehavior {

  public void fly() {
    stdout("Flying in the rocket");
  }
}
