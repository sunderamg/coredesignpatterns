package strategy;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 14, 2011
 * This class demonstrates the STRATEGY pattern. Notice that the Composition relationship is being used and
 * this is the hallmark of the STRATEGY pattern
 */
public abstract class Duck {
  //COMPOSE Duck with these two behaviors rather than inherit them
  FlyBehavior flyBehavior;
  QuackBehavior quackBehavior;

  public abstract void display();

  public void performFly() {
    flyBehavior.fly();
  }

  public void performQuack() {
    quackBehavior.quack();
  }

  public void swim() {
    stdout("All ducks swin and hence in the abstract class");
  }


  public void setQuackBehavior(QuackBehavior quackBehavior) {
    this.quackBehavior = quackBehavior;
  }

  public void setFlyBehavior(FlyBehavior flyBehavior) {
    this.flyBehavior = flyBehavior;
  }
}
