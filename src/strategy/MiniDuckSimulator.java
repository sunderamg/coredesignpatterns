package strategy;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 14, 2011
 * Class to exercise the SRATEGY PATTERN
 */
public class MiniDuckSimulator {

  public static void main(String[] args) {
    Duck mallard = new MallardDuck();
    mallard.display();
    mallard.swim();
    mallard.performFly(); //set the behaviors
    mallard.performQuack();
    //Inject behavior AT RUNTIME
    mallard.setFlyBehavior(new FlyWithRocket());
    mallard.setQuackBehavior(new MuteQuack());
    stdout("---------------Now changed behaviors are---------------------");
    mallard.performFly();
    mallard.performQuack();
  }
}
