package strategy;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: May 14, 2011
 * Time: 10:36:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class FlyWithWings implements FlyBehavior {

  public void fly() {
    stdout("I am flying high and will be always");
  }
}
