package strategy;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: May 14, 2011
 * Time: 10:33:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class MallardDuck extends Duck {

  public MallardDuck() {
   flyBehavior = new FlyWithWings(); //Inherited from the Duck base class
   quackBehavior = new Squeak();
  }

  @Override
  public void display() {
    stdout("Mallard Duck");
  }
}
