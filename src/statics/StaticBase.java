package statics;

import static log.Logger.stdout;

/**
  * User: gsunderam
  */
public class StaticBase {
    int var = 1;
    int base = 5;
    static int staticSuperVar = 10;

    public StaticBase() {
        stdout("var is " + var);
    }

    public static void meth() {
        stdout("Static super class meth " + staticSuperVar);
    }
}

class StaticDerv extends StaticBase {
    int var = 2; //shadowed base class variable. Variables are ALWAYS shadowed
    static int staticSuperVar = 20; //static shadowed variable, otherwise inherited

    StaticDerv() {
        stdout("sub var is " + var + " base variable " + base);
    }

//    public static void meth() {
//        stdout("sub static meth");
//    }

    public static void main(String[] args) {
        StaticBase base = new StaticDerv();
        stdout("Printing using base ref:shadowed var " + base.var); //REf type determines which var is called. Not overridden object type
        stdout("Printing using base ref:inherited var " + base.base); //
        stdout("Printing using base ref:static shadowed var " + base.staticSuperVar);
        base.meth(); //Static method calls
        StaticDerv derv = (StaticDerv) base;
        derv.meth(); //just a static method call like so StaticDerv.meth()
        stdout("Printing using derv ref:shadowed var " + derv.var);
        stdout("Printing using derv ref:inherited var " + derv.base);
        stdout("Printing using derv ref:shadowed var " + derv.staticSuperVar);

    }
}
