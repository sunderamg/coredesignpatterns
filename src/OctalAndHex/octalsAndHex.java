package OctalAndHex;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Dec 4, 2010
 * Demonstrates primtive types and casts in Java
 */
public class octalsAndHex {
    public static void main(String[] args) {
        int six = 06;
        int seven = 07;
        int eight = 010;
        int twelve = 014;
        stdout("Octal six: " + six + " Octal twelve: " + twelve);
        int x = 0x001;
        int y = 0x1ff;
        stdout("Hex 1: " + x + " y: " + y);
        char A = 65; //OK. less than 65535. No cast required
        char c1 = (char) 70000; //Cast required as 70000 >  65535. Allowed max for 16 bit unsigned char type
        stdout("A in ascii =" + A + " c1=" + c1);
        byte b = 10, b1 = 9;
        byte B = (byte) (b + b1); //If b & b1 are final, then cast is NOT required
        stdout("Byte B = " + B);
        B = 100 + 27; //OK byte can range from -128 through 127
        //byte I= 100 +28; //Needs to be int as value is 128;
        byte a_char = 'F' + 1; //Can add like this. F(ascii) + 1 will be 70 + 1 = 71
        stdout("byte char as int: " + a_char + " Ascii(byte char)=" + (char) a_char );
        a_char = 'F' + '\t'; //ascii codes will be added.
        stdout("Ascii addition of F and TAB: " + a_char);
        long longVal = 1000 ; //OK. implicit cast from int to long. widening conversion
        int intVal = (int) longVal; //Explicit cast required as we are narrowing from 64 bits to 32 bits
        intVal = (int) 345.34; //Explicit cast required from double to int due to possible loss of precision
        stdout("Double cast to int = " + intVal); //345
        //Interesting stuff
        longVal = 132;
        byte byteVal = (byte) longVal;
        stdout("byte equivalent of 132L " + byteVal);  //Bits to the left of lower 8 go away
        B = (byte) 128; //cast from int to byte
        stdout("The byte 128 is actually = " + B);  //represented as 32 bits originally as int. java lops the higher order 24 bits
                                                 //and retains the lower order 8 bits with the leftmost bit for "sign".
        String octal23 = Integer.toOctalString(23); //decimal to ocatal
        stdout("Octal 23 = " + octal23);
        String hex510 = Integer.toString(510, 16); //Another flavour, decimal and a radix 10, 8, 16
        stdout("Hex 510 = " + hex510); 
    }
}
