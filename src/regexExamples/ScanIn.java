package regexExamples;

import java.util.Scanner;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 2, 2010
 * Time: 10:39:51 AM
 * Scanner example from java.util
 */
public class ScanIn {
    public static void main(String[] args) {
        System.out.print("Input: ");
        System.out.flush();  //always flush after writing to a stream
        Scanner s = new Scanner(System.in);
        String token;
        do {
            token = s.findInLine(args[0]);
            System.out.println("Found match: " + token);
        } while (token != null);
        //print " and /
        System.out.println("\" \\");
        //tokenize on periods
        String source = "ab.cd.ef";
        /*
        The first \ is the escape sequence for the java compiler. So, \\. means "\." is interpreted literally by the compiler
        and is fed to the regex engine. The engine interprets as a literal DOT and NOT as a metacharacter DOT
        */
        String [] tokens = source.split("\\.");
        for (String alphabet : tokens) {
            System.out.println(alphabet);
        }
    }
}
