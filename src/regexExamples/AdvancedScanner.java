package regexExamples;

import log.Logger;

import java.util.Scanner;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 2, 2010
 * Time: 11:21:20 AM
 * Class that demonstrates Scanner methods such as nextInt, nextBoolean etc.
 */
public class AdvancedScanner {
    public static void main(String[] args) {
        Logger.stdout("String " + args[0]);
        boolean b2, b;
        int i;
        String s, hits = " ", hits2 = " ";
        Scanner s1 = new Scanner(args[0]);  //parse the source via args[0]
        Scanner s2 = new Scanner(args[0]);
        while (b = s1.hasNext()) {
            s = s1.next();
            hits += "s";    //simple output
        }
        while (s2.hasNext()) {
            if (s2.hasNextInt()) {
                i = s2.nextInt();
                hits2 += "i";
            } else if (s2.hasNextBoolean()) {
                b2 = s2.nextBoolean();
                hits2 += "b";
            } else {
                s2.next();
                hits2 += "s"; //Advanced output
            }
        }
        System.out.println("Scanner one: " + hits);
        System.out.println("Scanner two: " + hits2);
    }
}
