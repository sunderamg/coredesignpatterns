package regexExamples;

import log.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static log.Logger.log;
import static log.Logger.stdout;

/**
 * Date: Dec 1, 2010
 * Regular expr examples and formatting data
 */
public class RegexExample {
    public static void main(String [] args) {
        Pattern p = Pattern.compile("\\d*"); //zero or more: greedy quantifier
        Matcher m = p.matcher("ab34xs");
        Pattern p2 = Pattern.compile(".*?xx"); //using the reluctant quantifier produces different result
        Matcher m2 = p2.matcher("yyxxxyxx");  // ? means zero or once. so .* ? means one occurence each of any set of chars before xx
        //findMatches(m);
        stdout("---next-----");
        String includeRegex = "[\\s\\t]*<%@[\\s\\t]+include[\\sa-zA-Z=\"-/>]+";
        Pattern include = Pattern.compile(includeRegex);
        Matcher includeMatcher = include.matcher("<%@ include file=\"/WEB-INF/jsp/includes/includes.jsp\" %>");
        findMatches( includeMatcher);

        String jspIncludeRegex = "[\\s\\t]*<jsp:include[\\s]+[\\sa-zA-Z\"=>-]+\n</jsp:include>[\\s\\t]*";
//        String jspIncludeRegex = "[\\s\\t]*<jsp:include[\\s]+[\\sa-zA-Z\"=>-]+";
        StringBuilder jspIncludeInput = new StringBuilder("<jsp:include attrib=\"value\" attrib=\"value\">");
        jspIncludeInput.append("\n");
        jspIncludeInput.append("</jsp:include>");
        Pattern jspIncludePattern = Pattern.compile(jspIncludeRegex);
        Matcher jspIncludeMatcher = jspIncludePattern.matcher(jspIncludeInput);
        boolean isFound = findMatches(jspIncludeMatcher);
            
//        while (m2.find()) {
//            stdout(new StringBuilder().append(m2.start()).append(" ").append(m2.group()).toString());
//        }
//        //Formatting using C-Style printf function
//        System.out.printf("%1$d + %2$d \n", 123, 321);
//        /**
//         *  %[arg_index$] [flags] [width] [.precision for f numbers]conversion char
//         */
//        int i1 = -123;
//        int i2 = 45692;
//        System.out.printf(">%1$5d< \n", i1);
//        System.out.printf(">%+-(7d< \n", i2);  //flag - is left justify, + isto include sign, (
//        System.out.printf("%0,7d \n", i2);      //says negative numbers needs to be included in ()
//        System.out.printf("%2$d + %1$b \n", true, i2);
//        System.out.format("%s", new Long("123"));
//        System.out.printf("%b", 123);
    }

  private static boolean findMatches(Matcher m) {
    boolean isFound = false;
    while (m.find()) {
        System.out.print(new StringBuilder().append(m.start()).append(" ").append(m.group()).append("\n").toString());
        isFound = true;
    }
    
    return isFound;
  }
}
