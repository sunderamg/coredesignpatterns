package collectionUtils;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Oct 26, 2010
 * Time: 12:27:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class NameSort implements Comparator<DvdInfo> {
    
    public int compare(DvdInfo o1, DvdInfo o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
