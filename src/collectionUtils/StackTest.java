package collectionUtils;

import java.util.Stack;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Jun 18, 2011
 */
public class StackTest {
  public static void main(String[] args) {
    Stack<String> words = new Stack<String>();
    words.push("G Sunderam");
    words.push("Will succeed");
    words.push("And will get good wife");
    words.push("Green ahead");

    reviewStack(words);
  }

  private static void reviewStack(Stack<String> words) {
    while (!words.empty()) {
      stdout(words.peek());
      stdout(words.pop());
    }

  }
}
