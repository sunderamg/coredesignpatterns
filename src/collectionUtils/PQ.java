package collectionUtils;

import log.Logger;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 18, 2010
 * Time: 9:33:33 PM
 * Priority Queue class demo
 */
public class PQ {

    static class PQSort implements Comparator<Integer> {
        public int compare(Integer o1, Integer o2) {
            return o1 - o2;
        }
    }
    
    public static void main(String[] args) {
        int [] a = {10, 8, 11, 6, 3};
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();

        loadQueue(a, pq); //sorts by natural order
        reviewQueue(pq);
        //sort using a comparator
        PriorityQueue<Integer> pQ = new PriorityQueue<Integer>(10, new PQSort());
        //load queue again
        loadQueue(a, pQ);  //sorted by priority determined by this comparator      
        //reviewQueue(pQ);
//        pQ.peek();
        stdout("size before peek: " + pQ.size());
        stdout(pQ.peek());
        stdout("size after peek: " + pQ.size());
        stdout(pQ.poll()); //removes the head or the top priority element
        stdout("size after poll: " + pQ.size());
        reviewQueue(pQ);
        PriorityQueue<String> pq2 = new PriorityQueue<String>();
        String [] sa = {" f", " F", "FF", "ff"};
        for (String s :  sa) {
            pq2.offer(s);
        }
        stdout("\n");
        for (String s : sa)  System.out.print(pq2.poll() + ",");
    }

    private static void reviewQueue(PriorityQueue<Integer> pq) {
        for (int i =0; i < 5; i++) {  //Arranges per the natural ordering
            System.out.print(pq.poll() + " ");
        }
    }

    private static void loadQueue(int[] a, PriorityQueue<Integer> pq) {
        for (int i : a) {
            pq.offer(i);
        }
    }
}
