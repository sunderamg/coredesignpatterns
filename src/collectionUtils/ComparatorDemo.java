package collectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Oct 26, 2010
 * Time: 12:05:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComparatorDemo {
     public static void main(String args []) {
        List<DvdInfo> list = new ArrayList<DvdInfo>();
        list.add(new DvdInfo("GS", "Success", "GS"));
        list.add(new DvdInfo("Sun", "Sunnyvale", "GS"));
        list.add(new DvdInfo("SVNIT", "NITSurat", "GS"));
        list.add(new DvdInfo("Sankara", "Highschool", "GS"));
        list.add(new DvdInfo("Computerpoint", "Privateinstitute", "GS"));
        Collections.sort(list);
        System.out.println("Sorting by title via comparable");
        System.out.println(list);
        System.out.println("Sorting by name via comparator");
        NameSort ns = new NameSort();
        Collections.sort(list, ns);
        System.out.println(list);

     }
}

class DvdInfo implements Comparable<DvdInfo> {

    private String name;

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getLeadActor() {
        return leadActor;
    }

    private String title;
    private String leadActor;

    DvdInfo(String name, String title, String leadActor) {
       this.name = name;
       this.title = title;
       this.leadActor = leadActor;
    }

    public String toString() {
        return name + " " + title + " " + leadActor;
    }

    public int compareTo(DvdInfo o) {
        return title.compareTo(o.title);  
    }
}