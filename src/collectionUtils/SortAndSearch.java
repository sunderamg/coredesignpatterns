package collectionUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static log.Logger.*;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 18, 2010
 * Time: 1:05:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class SortAndSearch {
    public static void main(String[] args) {
        String [] a = {"one","two", "three","four"};
        Arrays.sort(a);    //Sort by natural ordering
        printElements(a);
        stdout(Arrays.binarySearch(a, "one")); //returns 1
        //NumberSort ns = new NumberSort(); //Reverse sort
        Comparator<Object> comp = Collections.reverseOrder();
        Arrays.sort(a, comp); //Same reverse sort accomplished via Collections utils method
        printElements(a);
        stdout(Arrays.binarySearch(a, "one"));  //yields incorrect result
        stdout(Arrays.binarySearch(a, "one", comp)); //sort and search citeria MUST be same.
        List<String> stringList = Arrays.asList(a);
        stringList.set(0, "Five");  //changing either of the list or the array elements
        a[1] = "six"; //changes the other! See below
        stdout("Printing list elements...");
        for (String s : stringList) {
            System.out.print(s + " ");
        }
        stdout("\nprint array elements...");
        printElements(a);
    }

    private static void printElements(String[] a) {
        for (String s : a) {
            System.out.print(s + " ");
        }
    }
}

class NumberSort implements Comparator<String> {

    public int compare(String s1, String s2) {
        return s2.compareTo(s1);
    }
}