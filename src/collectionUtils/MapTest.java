package collectionUtils;

import java.util.HashMap;
import java.util.Map;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Dec 18, 2010
 */
public class MapTest {
    public static void main(String[] args) {
        Map<Object, Object> m = new HashMap<Object, Object>();
        m.put("k1", new Goat("aiko"));
        m.put("k2", Pets.GOAT);
        m.put(Pets.CAT, "CAT Key");
        Goat d1 = new Goat("clover");
        m.put(d1, "Goat Key");
        m.put(new Cat(), "Cat Key");

        stdout("k1 = " + m.get("k1"));
        stdout("k2 = " + m.get("k2"));
        stdout("d1 = " + m.get(d1));
        stdout("enum cat = " + m.get(Pets.CAT));
        stdout("No equals cat = " + m.get(new Cat()));  //null as equals is not overridden
        //change state of d1
        stdout("Changing state now");
        //TODO: Visit this to get more clarity into the HashMap.put(Object) and get(Object) methods
        d1.name = "magnolia";
        stdout("Goat key after changing hascode:length 8 " + m.get(d1));
        d1.name="clover";
        stdout("Goat key after changing hascode:length 6 " + m.get(new Goat("clover")));
        d1.name = "clover";
        stdout("Goat key after changing hascode:length 6 " + m.get(new Goat("aarthur")));
    }
}

enum Pets {GOAT, CAT, HORSE}
class Goat {
    String name;

    Goat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean equals(Object o) {
        stdout("EQUALS called for " + name);
        if (o instanceof Goat && ((Goat) o).name.equalsIgnoreCase(name)) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        stdout("Hashcode called for " + name);
        return name.length();
    }

    public String toString() {
        return name;
    }
}
 class Cat {
//     public boolean equals(Object o) {
//         return true;
//     }
//
//     public int hashCode() {
//         return 1;
//     }
 }