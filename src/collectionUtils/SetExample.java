package collectionUtils;

import java.util.Set;
import java.util.TreeSet;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Mar 27, 2011
 * Time: 4:01:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetExample {

    public static void main(String[] args) {
        Set<Number> s = new TreeSet<Number>();
        s.add(107); //Can't add Integer and Long as they are NOT comparable
        s.add(new Integer("42").intValue());
        s.add(null);
        stdout("Size of set = " + s.size());
    }
}
