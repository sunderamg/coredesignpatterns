package collectionUtils;

import java.util.concurrent.CountDownLatch;

import static log.Logger.log;

/**
 * TestHarness
 * <p/>
 * Using CountDownLatch for starting and stopping threads in timing tests
 * @author Brian Goetz and Tim Peierls
 */
public class TestHarness {
    public long timeTasks(int nThreads, final Runnable task)
            throws InterruptedException {
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread("THREAD " + i) {
                public void run() {
                    try {
                        log(this.getName() + " awaiting");
                        startGate.await();       //Prevent any one thread from getting a "Head Start"
                        try {
                            task.run();
                        } finally {
                            log(this.getName() + " countdown beginning");
                            endGate.countDown();
                        }
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                }
            };
            t.start();
        }

        long start = System.nanoTime();
        log("Counting down on start gate");
        startGate.countDown();
        endGate.await();
        log("End gate await over");
        long end = System.nanoTime();
        return end - start;
    }

    public static void main(String args[]) throws InterruptedException {
        TestHarness th = new TestHarness();
        Runnable task = new Runnable() {
             public void run() {
                log("Running task");
            }
        };
        long t1 = System.nanoTime();
        long timetaken = th.timeTasks(10, task);
        long t2 = System.nanoTime();
        log("Time Taken for all ten threads to run PARALLEL " + timetaken);
    }
}

