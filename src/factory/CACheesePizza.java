package factory;

import simplefactory.Pizza;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class CACheesePizza extends Pizza {

  public CACheesePizza() {
    name = "CA Cheese pizza";
    dough="Thin crust for CA";

    toppings.add("CA Grape Fruit");
  }
}
