package factory;

import simplefactory.Pizza;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class CAVeggiePizza extends Pizza {

  public CAVeggiePizza() {
    name = "Caifornia Veggie pizza";
    dough="Thick crust for CA too";

    toppings.add("Veggie California Items");
  }

   @Override
   public void bake() {
    stdout("California Veggie Pizza is BAKED differently");
  }

  @Override
  public void cut() {
    stdout("California Veggie Pizza is cut in to rectangles");
  }
}
