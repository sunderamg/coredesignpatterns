package factory;

import abstractfactory.AbstractPizza;
import simplefactory.Pizza;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public abstract class NewPizzaStore {

  public Pizza orderPizza(String type) {
    Pizza pizza = createPizza(type); //A specific type of Pizza is returned

    pizza.prepare(); //std base method
    pizza.bake(); //polymorphic based on type returned in createPizza
    pizza.cut();  //polymorphic based on type returned in createPizza
    pizza.box();//std base method

    return pizza;
  }

  protected abstract Pizza createPizza(String type);
  
}
