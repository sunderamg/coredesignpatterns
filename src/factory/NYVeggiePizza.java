package factory;

import simplefactory.Pizza;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class NYVeggiePizza extends Pizza {

  public NYVeggiePizza() {
    name = "NewYork Veggie pizza";
    dough="Thick crust for NY";

    toppings.add("Veggie NY Items");
  }

  public void cut() {
    stdout("NY Veggie Pizza is cut in to squares");
  }
}
