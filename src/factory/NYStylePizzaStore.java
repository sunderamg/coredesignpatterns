package factory;

import simplefactory.*;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class NYStylePizzaStore extends NewPizzaStore {

  PizzaType pizzaType;

  @Override
  public Pizza createPizza(String type) {
    Pizza pizza = null;

    if (PizzaType.getEnumByType(type) == pizzaType.CHEESE) {
      pizza = new NYCheesePizza();
    } else if (PizzaType.getEnumByType(type) == pizzaType.VEGGIE) {
      pizza = new NYVeggiePizza();
    }

    return pizza;
  }
}
