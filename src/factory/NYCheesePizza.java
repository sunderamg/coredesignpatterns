package factory;

import simplefactory.Pizza;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class NYCheesePizza extends Pizza {

  public NYCheesePizza() {
    name = "NewYork Cheese pizza";
    dough="Thin crust for NY";

    toppings.add("NYFruits");
  }

  public void bake() {
    stdout("NY Pizza is baked at a higher temperature");
  }

}
