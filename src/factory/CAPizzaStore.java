package factory;

import simplefactory.Pizza;
import simplefactory.PizzaType;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class CAPizzaStore extends NewPizzaStore {

  PizzaType pizzaType;

  @Override
  protected Pizza createPizza(String type) {
     Pizza pizza = null;

    if (PizzaType.getEnumByType(type) == pizzaType.CHEESE) {
      pizza = new CACheesePizza();
    } else if (PizzaType.getEnumByType(type) == pizzaType.VEGGIE) {
      pizza = new CAVeggiePizza();
    }

    return pizza;
  }
}
