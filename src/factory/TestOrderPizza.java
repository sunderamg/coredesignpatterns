package factory;

import simplefactory.Pizza;
import simplefactory.PizzaStore;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 * 
 * Contains two factory heirarchies. ProductFactory and product. Each product factory is responsible for creating a
 * its own specific product
 * 1) abstract base factpory = new Specific factory
 * 2) basefactory.callBasemethod
 * 3) Inside that base class method, product specific to specific factory is created and returned.
 *    polymorphism is INSIDE this method. createPizza
 * 4) Process the returned product in 3 to further refine it per product heirarchy
 */
public class TestOrderPizza {

  public static void main(String[] args) {
    NewPizzaStore nyStore = new NYStylePizzaStore();  //Factory of stores
    Pizza nyPizza = nyStore.orderPizza("cheese"); //factory of products
    stdout("Name of ny pizza " + nyPizza.name + " Toppings " + nyPizza.toppings.get(0));

    NewPizzaStore caStore = new CAPizzaStore();
    Pizza caPizza = caStore.orderPizza("veggie");
    stdout("Name of CA pizza " + caPizza.name + " Toppings " + caPizza.toppings.get(0));
  }


}
