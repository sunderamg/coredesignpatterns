package observer;

import java.util.ArrayList;
/**
 * User: gsunderam
 * Date: May 17, 2011
 */
public class WeatherData implements Subject {
 private ArrayList<Observer> observers = new ArrayList<Observer>();
 private float humidity;
 private float pressure;
 private float temperature;

 public void measurementsChanged() {
   notifyObservers();
 }

  public void addObserver(Observer o) {
    observers.add(o);
  }

  public void removeObserver(Observer o) {
    if (observers.indexOf(o) >= 0) {
      observers.remove(o);
    }
  }

  public void notifyObservers() {
    for(Observer observer : observers) {
      observer.update(humidity, temperature, pressure);
    }
  }

  public void setMeasurements(float humidity, float temperature, float pressure) {
    this.humidity = humidity;
    this.temperature = temperature;
    this.pressure = pressure;
    measurementsChanged();
  }
}
