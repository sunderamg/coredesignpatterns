package observer;

/**
 * User: gsunderam
 * Date: May 17, 2011
 * Test Harness to exercise the observer pattern
 */
public class WeatherStation {
  public static void main(String[] args) {
    WeatherData weatherData = new WeatherData();
    CurrentConditionsDisplay currentConditions = new CurrentConditionsDisplay(weatherData);
    StatisticsDisplay satistics = new StatisticsDisplay(weatherData);

    //This one line calls the update method of the TWO observers
    weatherData.setMeasurements(33, 23, 45f);
  }
}
