package observer;

/**
 * User: gsunderam
 * Date: May 17, 2011
 */
public interface Observer {
  public void update(float humidity, float temperature, float pressure);
}
