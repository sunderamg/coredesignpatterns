package observer;

/**
 * User: gsunderam
 * Date: May 17, 2011
 */
public interface Subject {
  public void addObserver(Observer o);
  public void removeObserver(Observer o);
  public void notifyObservers();
}
