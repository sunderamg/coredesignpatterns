package observer;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 19, 2011
 */
public class StatisticsDisplay implements Observer, DisplayElement {

  private Subject weatherData;
  private float temperature;
  private float humidity;
  private float pressure;

  public StatisticsDisplay(Subject weatherData) {
    this.weatherData = weatherData;
    weatherData.addObserver(this);
  }

  public void display() {
    stdout("-------------------------Statistics display is below---------------");
    stdout("Current Statistics are Temperature: " + temperature + " Humidity: " + humidity + " Pressue: " + pressure);
  }

  public void update(float humidity, float temperature, float pressure) {
    this.temperature = temperature;
    this.humidity = humidity;
    this.pressure = pressure;
    display();
  }
}
