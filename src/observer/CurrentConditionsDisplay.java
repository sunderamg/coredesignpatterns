package observer;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 17, 2011
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
  private Subject weatherData;
  private float temperature;
  private float humidity;

  public CurrentConditionsDisplay(Subject weatherData) {
      this.weatherData = weatherData;
      weatherData.addObserver(this);
  }

  public void display() {
    stdout("-------------------------Current conditions display is below---------------");
    stdout("The current conditions are Temperature: " + temperature + " Humidity: " + humidity);
  }

  public void update(float humidity, float temperature, float pressure) {
    this.temperature = temperature;
    this.humidity = humidity;
    display();
  }
}
