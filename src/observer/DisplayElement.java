package observer;

/**
 * User: gsunderam
 * Date: May 17, 2011
 */
public interface DisplayElement {
  public void display();
}
