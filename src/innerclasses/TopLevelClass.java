package innerclasses;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 26, 2010
 * Time: 10:15:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopLevelClass {

    private int instanceVar = 5;
    private static int staticVar = 6;

    public static void main(String[] args) {
        TopLevelClass outer = new TopLevelClass();
        outer.callInner();
        Inner in = outer.new Inner(); //Instantiate Inner class from outside of an instance method
        in.seeOuter();   //Need an outer instance to make an inner!
        StaticInnerClass sic = new StaticInnerClass();  //No "this" reference OR any other reference of the outer class needed.
        sic.statInner();
    }

    public void callInner() {
        Inner inner = new Inner(); //Invoke inner class from within an instance method
        inner.seeOuter();
    }

    class Inner {
        public void seeOuter() {
            stdout("Outer class var from within inner class instanceVar " + instanceVar);
            stdout("Outer class reference: " + TopLevelClass.this);
        }
    }

    static class StaticInnerClass {
        public void statInner() {
            stdout("Static variable from within inner class " + staticVar);
        }
    }

}
