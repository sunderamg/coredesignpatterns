package innerclasses;

import log.Logger;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 26, 2010
 * Time: 10:59:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class MethodLocal {

    private int a = 'a';
    private static String classVar = "inner class variable";

    public static void main(String[] args) {
        MethodLocal local = new MethodLocal();
        local.go(5, "hello");
    }

    public void go(final int c, String s) {
        final int localVar = 8;
        int normalVar = 9;
        
        class MethInner {
           public void seeOuter(int d) {
               stdout("Outer instance var a " + a); //Ascii value will be printed #97
               stdout("final local var " + localVar);
               stdout("final local var " + classVar); //can access static fields of the enclosing class
               stdout("formal arg " + d);
               //stdout("Non final var " + normalVar); //CAN'T access s and normalVar as it's non final!
           }
        }
        MethInner inner = new MethInner();
        inner.seeOuter(4);
    }
}
