package innerclasses;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 27, 2010
 * Time: 9:02:36 PM
 * To change this template use File | Settings | File Templates.
 */
interface Foo {
    public String doStuff();
}

class Bar {
    public void buyDrink(Foo f) {
        stdout("bought drink " + "\n" + "Now calling foo: " + f.doStuff());
    }
}
class Popcorn {
    public Popcorn() {
        stdout("Super class constructor");
        foo();  //calls the subclass anonymous popcorn - polymorphism here! runtime binding to subclass
    }

    public void foo() {
        stdout("super class popcorn");
    }
}

public class Food {
    public Food() {
        stdout("Food constructor");
    }

    Popcorn p = new Popcorn() {
       public void foo() {
           stdout("Foo: Anonymous subclass popcorn");
       }
       public void bar() { //can't call this using the reference p
            stdout("Bar another method");
        }
    };

    public void fooIt() {
        p.foo();
        //p.bar();  //can't invoke bar as its NOT in the superclass
    }

    public static void main(String[] args) {
        //Anonymous inner class flavour #1
        new Food().fooIt();
        //Illustration of another flavour of inner classes - argument inner classes - #2
        Bar b = new Bar();
        b.buyDrink(new Foo() {
             public String doStuff() {
                return "Completed assigments";  //To change body of implemented methods use File | Settings | File Templates.
            }
        }); //always end like this 
    }
}
