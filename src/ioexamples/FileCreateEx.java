package ioexamples;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import static log.Logger.stdout;

/**
 * Date: Dec 2, 2010
 * usage of file class in real life
 */
public class FileCreateEx {
    public static void main(String[] args) {
        File file1 = new File("src\\log","Logger.java");
        stdout("Does exist: " + file1.isFile());
        File dir = new File("test");
        dir.mkdir();
        File dirFile = new File(dir, "testFile.txt");
        stdout("IS exists() " + dirFile.isFile());
        PrintWriter pw = null; //printwriter is really cool. can use File as its argument in java 5
        try {
            dirFile.createNewFile();
            stdout("IS exists() " + dirFile.isFile());
            pw = new PrintWriter(dirFile);
            pw.println("My name is gsunderam");
            pw.println("I am successful in a java 2 exam");
            pw.printf("%2$+,09d is just a number", 323, 434); // can use printf on the pw
            pw.append("Got a clean build");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pw != null) pw.close();
        }
        stdout(dir.delete());
        dirFile.renameTo(new File(dir, "testFile1.txt"));
        dir.renameTo(new File("testDIR"));
    }
}
