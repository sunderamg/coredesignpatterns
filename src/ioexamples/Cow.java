package ioexamples;

import java.io.*;

import static log.Logger.stdout;

/**
 * Externalizable was provided since java 1.1 because the traditional serialization was slow due to reflection.
 * But as Java progressed the Default Serial mech. improved as a result of improvement in reflaction.
 *
 * 1) Use externalizable to provide markedly different deserialization scheme and to have complete control over the
 *    serialization process.
 * 2) Use it store data in a different encoding- like "Y" for true.
 * 3) Use it if all of the class metadata upon desrialization is not needed. The default java serialization
 *    stores a bulk of metadata that could degrade serialization performance slightly.
 * 4) Finally some of the pros for Externalizable could also be achievable via readObject and writeObject custom serialization
 *    that comes with java.
 * 5) Cons: If the class defination changes, then one needs to change the read and write external methods. This is big
 *    minus for externalization.
 */
public class Cow implements Externalizable {
   int weight;
   static int z = 9;
   transient int x = 10;
   String name;

	 public Cow() { super(); }

   public Cow(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		stdout("Calling write external...");
    out.writeUTF(name); //UTF is better for ASCII characters. Better than readObject
		out.writeInt(weight);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		stdout("Calling read external...");
    name = in.readUTF();
		weight = in.readInt();
	}
}

