package ioexamples;

import java.io.File;

import static log.Logger.log;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 30, 2010
 * Time: 4:28:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileSearchExample {

    public static void main(String [] args) {
        //Search for files in a directory
            System.out.println("File searching example");
            System.out.println("-----------------------");
            listDirFiles(new File("src"));
    }

    private static void listDirFiles(File file) {
        //File file = new File(filename);
        if (file.isDirectory()) {
            System.out.println("The Directory name is " + file.getName());
            File [] files;
            files = file.listFiles();
            //System.out.println(files.length);
            for (File f : files) {
                if (f.isFile()) {
                    System.out.println("The file name is " + f.getName());
                } else  {
                    listDirFiles(f);
                }
            }
        }
    }
}
