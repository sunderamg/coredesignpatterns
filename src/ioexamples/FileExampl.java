package ioexamples;

import java.io.*;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 17, 2010
 * Time: 9:48:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileExampl {

    public static void main(String [] args) {
        File file = new File("temp.txt");
        FileWriter fw = null;PrintWriter pw = null;
        BufferedReader br = null;
        try {
            stdout(file.createNewFile());
            fw = new FileWriter(file);
            pw = new PrintWriter(fw);
            pw.print("Success");
            pw.write(" for GSunderam\n");
            pw.flush();pw.close();   //Always flush after file writes
            FileReader fr = new FileReader(file);
            br = new BufferedReader(fr);
            String s;
            while((s = br.readLine()) != null) {
                stdout(s);
            }
        } catch (IOException e) {
            stdout(e.getMessage());
        }
        finally {
            //assert pw != null;
           // pw.close();
            try {
                br.close();
               // fw.close();
                
            } catch (IOException e) {
                e.printStackTrace(); 
            }
        }
    }

    
}
