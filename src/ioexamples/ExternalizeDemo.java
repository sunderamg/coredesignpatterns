package ioexamples;

import java.io.*;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Oct 23, 2016
 */
public class ExternalizeDemo {
	private static final String FILENAME = "externalizable.ser";

	public static void main(String[] args) throws Exception {
		Cow cow = new Cow("kamadenu", 40);

    FileOutputStream fos = new FileOutputStream(FILENAME);
    ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(cow);
    oos.flush();
    oos.close();

    FileInputStream fis = new FileInputStream(FILENAME);
    ObjectInputStream ois = new ObjectInputStream(fis);
    Cow newCow = (Cow)ois.readObject();
    ois.close();

    stdout("Cow name: " + newCow.name + " Transient var x" + newCow.x + " Static z: " + newCow.z);
	}
}
