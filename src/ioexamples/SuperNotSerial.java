package ioexamples;

import log.Logger;

import java.io.*;

import static log.Logger.log;
import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 30, 2010
 * Time: 7:56:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class SuperNotSerial {
    public static void main(String [] args) {
       Dog d = new Dog("Dawg", 23);
       ++d.z;
       stdout("Before:Weight " + d.weight + " Name: " + d.name);
       try {
           FileOutputStream fos = new FileOutputStream("testSer.ser");
           ObjectOutputStream os = new ObjectOutputStream(fos);
           os.writeObject(d);
           os.close();
       } catch (Exception e) { e.printStackTrace();}
        try {
            FileInputStream fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Dog dog = (Dog) ois.readObject();
            ois.close();
            stdout("After:Weight " + dog.weight + " Name: " + dog.name + " x = " + dog.x + " z = " + dog.z);
        } catch (Exception e) { e.printStackTrace(); }
    }
}

class Dog extends Animal implements Serializable {
   int weight;
   static int z = 9;
   transient int x = 10;
   //String name;

   Dog(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }
}

class Animal {
    //int weight = 43;
    String name = "Initial";
}
