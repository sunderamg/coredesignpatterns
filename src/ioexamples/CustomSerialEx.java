package ioexamples;

import log.Logger;

import java.io.*;

import static log.Logger.*;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 30, 2010
 * Time: 10:02:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomSerialEx {
    public static void main(String [] args) {
        Collar collar = new Collar(3);
        Cat cat = new Cat(collar, "pussy");
        //serialization code
        try {
            FileOutputStream fos = new FileOutputStream("testCatSer.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(cat);
            oos.close();
            log("Collar size BEFORE: " + cat.getCollar().getSize());
        } catch(IOException ioe) { ioe.printStackTrace(); }
        try {
            FileInputStream fis = new FileInputStream("testCatSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Cat c = (Cat) ois.readObject();
            ois.close();
            log("Collar size After: " + c.getCollar().getSize() + " Name: " + c.getName());
        } catch(Exception ioe) { ioe.printStackTrace(); }

    }
}

class Cat implements Serializable {
    private transient Collar collar;
    private String name;

    public Collar getCollar() {
        return collar;
    }

    public String getName() {
        return name;
    }

    public Cat(Collar collar, String name) {
        this.collar = collar;
        this.name = name;
    }

    /**
     * Custom serialization using write object in the API
     * @param oos
     */
    private void writeObject(ObjectOutputStream oos) {
        stdout("Serializing...");
        try {
            oos.defaultWriteObject();
            oos.writeInt(collar.getSize());   //write the integer into the stream
        } catch (IOException e) { e.printStackTrace();  }
    }

    /**
     * custom deserialization using read objectr in the API
     * @param ois
     */
    private void readObject(ObjectInputStream ois) {
        stdout("Deserializing...");
        try {
            ois.defaultReadObject();
            int collarSize = ois.readInt(); //read the int back from the stream
            this.collar = new Collar(collarSize); //create a collar using the read int and assign it to the instance field
        } catch (IOException e) { e.printStackTrace();}
          catch (ClassNotFoundException e) { e.printStackTrace(); }
    }

  /**
   * This method if provided will be called after default serialization in lieu of returning the object
   * in a regular way. 
   */
//	public Object readResolve() {
//		stdout("Inside read resolve");
//		return new Cat(new Collar(4), "jimmy");
//	}
}

class Collar implements Serializable {    //Error if this doesn't implement serializable
    private int size;

    public int getSize() {
        return size;
    }

    public Collar(int size) {
        this.size = size;
    }
}
