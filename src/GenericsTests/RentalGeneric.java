package GenericsTests;

import log.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 24, 2010
 * Time: 7:50:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class RentalGeneric<T> {

    private List<T> rentableItems;
    int number;

    public RentalGeneric(int number, List<T> rentableItems) {
        this.number = number;
        this.rentableItems = rentableItems;
    }

    public T getRental() {
        return rentableItems.get(0);
    }

    public void returnRental(T item) {
        rentableItems.add(item);
    }
}

class TestRental {
    public static void main(String[] args) {
       Car c1 = new Car();
        c1.setName("volvo");
       Car c2 = new Car();
        c2.setName("Honda");
       List<Car> carPool = new ArrayList<Car>();
        carPool.add(c1);
        carPool.add(c2);
       RentalGeneric<Car> rg = new RentalGeneric<Car>(5, carPool);
       Car carToRent = carPool.get(0);
       Logger.stdout("Car rented : " + carToRent.getName());
       rg.returnRental(c1);
    }
}

class Car{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;


}
