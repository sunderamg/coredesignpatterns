package GenericsTests;

import java.util.HashSet;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Oct 26, 2010
 */
public class Group extends HashSet<Person> {

    public static void main(String [] args) throws Exception {
        Group g = new Group();          
        stdout(g.add(new Person("GS")));
        stdout(g.add(new Person("Sun")));
        stdout(g.add(new Person("Moon")));
        stdout(g.add(new Person("GS")));
        System.out.println("Size: " + g.size());
        // p1.callMeth(log);     //Using instance var to access static variable
        Object person = new Person("ObjectPerson");
        int objectNumber = g.findGroupNumber(person);//reference type determines that Object version is called
        stdout("Object number " + objectNumber);
        Person p = (Person) person;
        int personNumber = g.findGroupNumber(p);//person version is called
        stdout("Person number " + personNumber);
    }

    // public boolean add(Object o) { //Illegal override due to type erasure
    public boolean add(Person o){ //Legal override
        return super.add(o);
    }

    //These two are over loaded methods
    public int findGroupNumber(Object o) {
        return 1;
    }

    public int findGroupNumber(Person o) {
        return 2;
    }
}

class Person {
    public Person(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public boolean equals(Object o) {
        stdout("Checking for equality " + ((Person) o).name);
        if (o instanceof Person) {
            return ((Person) o).name == this.name;
        }
        return false;
    }

    public int hashCode() {
        stdout("Checking in the bucket " + name.length());
        return name.length();
    }

    public static void callMeth(log.Logger log) {
        //log.Logger log = new Logger();
        log.log("GenericsTests", "here");
    }
    private final String name;
}
