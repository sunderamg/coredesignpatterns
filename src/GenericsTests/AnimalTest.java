package GenericsTests;

import java.util.ArrayList;
import java.util.List;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Oct 26, 2010
 * Time: 3:19:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class AnimalTest {
    public static void main(String [] args) {
        List<Dog> l = new ArrayList<Dog>();
        l.add(new Dog("dummy"));
        addAnimal(l);
        List<Animal> l2 = new ArrayList<Animal>();
        l2.add(new Dog());   //This is fine as with Arrays
        addAnimalCorrectly(l2);
        List<Object> l3 = new ArrayList<Object>();
        l3.add(new Dog("blue"));
        addAnimalCorrectly(l3);
        Dog d = new Dog("pussy");
        makeList(d);
        //fun with array reference assigments
        Animal [] animals;
        Dog dogs [] = new Dog[5];
        animals = dogs; //ok to assign subtypes in case of array references
        animals[0] = new Dog("pupy");  //can't do this with generics!
        for (Animal dog : animals) stdout(dog);
    }

    /**
     * extends keyword below allows to pass subtypes of the "Type" after extends keyword
     * @param animal
     */
    static void addAnimal(List<? extends Animal> animal) {
        //animal.add(new Cat());
        System.out.println("Size: " + animal.size());
    }

    /**
     * super keyword below allows to add elements ABOVE Cat level. Cat, Animal, Object
     * @param animal
     */
    static void addAnimalCorrectly(List<? super Cat> animal) {
        animal.add(new Cat());
        Object dog = animal.get(0); // Ok just to get object
//        Cat dog2 = animal.get(0); // Error can only get object from super type
        System.out.println("animal 0 " + dog);
        System.out.println("Size: " + animal.size());
    }

    /**
     * Generic method type arguments. just declare E BEFORE the return type
     * wildcard "?" not allowed in place of E
     * @param e
     * @param <E>
     */
    public static <E> void makeList(E e) {
        List<E> list = new ArrayList<E>();
        list.add(e);
    }
}

class Animal{}

class Dog extends Animal {
    String name;
    Dog() {}

    Dog(String name) {
        this.name = name;
    }
}

class Cat extends Animal{}
