package GenericsTests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Mar 28, 2011
 * Time: 10:59:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenReturnType {

    public <T> Collection<? super T> getDummyList(T t) {
        List<T> list = new ArrayList<T>();
        list.add(t);
        return list;
    }

     public <E> List<? extends E> passDummyList(List<E> list) {
        stdout("Size of incoming list " + list.size());
        List<E> newList = new ArrayList<E>(10);
        newList.add(list.get(0));
        return newList;
    }

    public static void main(String[] args) {
        GenReturnType test = new GenReturnType();
        Collection<?> list = test.getDummyList("34");
        stdout(list.size()); //read only list as there is no super
        String [] names = {"GS", "Always", "Succeeds", "in", "every", "endeavor"};
        List<?> newNameList = test.passDummyList(Arrays.asList(names));
        //newNameList.add("Good job"); //Mutable list because of super
        stdout("Size of new list " + newNameList.size());
    }
}
