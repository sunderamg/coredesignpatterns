package simplefactory;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class SimplePizzaFactory {

  private PizzaType pizzaType;

  public Pizza createPizza(String type) throws Exception {
    if (type == null) {
      throw new Exception("Incorrect pizza type");
    }

    Pizza pizza = null;

    if (PizzaType.getEnumByType(type) == pizzaType.CHEESE) {
      pizza = new CheesePizza();
    } else if (PizzaType.getEnumByType(type) == pizzaType.VEGGIE) {
      pizza = new VeggiePizza();
    }

    return pizza;
  }
}
