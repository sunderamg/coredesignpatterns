package simplefactory;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class TestPizza {
  public static void main(String[] args) throws Exception {
    SimplePizzaFactory factory = new SimplePizzaFactory();
    PizzaStore store = new PizzaStore(factory);

    Pizza pizza = store.orderPizza("veggie");
    stdout(pizza.name);
  }
}
