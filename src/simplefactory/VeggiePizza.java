package simplefactory;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class VeggiePizza extends Pizza {
  
   public VeggiePizza() {
    name = "Veggie pizza";
    dough = "Thick crust";
     
    toppings.add("Veggie ha ha");
  }

  public void cut() {
    stdout("special cutting for Veggie pizza");
  }
}
