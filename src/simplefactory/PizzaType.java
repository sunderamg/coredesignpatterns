package simplefactory;

import java.util.HashMap;
import java.util.Map;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public enum PizzaType {

  CHEESE("cheese"), VEGGIE("veggie");
  
  private static Map<String, PizzaType> map = new HashMap<String, PizzaType>();

  static {
    for (PizzaType pizza : values()) {
       map.put(pizza.getType(), pizza);
    }
  }

  public String getType() {
    return type;
  }

  private String type;

  PizzaType(String type) {
    this.type = type;
  }

  public static Enum getEnumByType(String type) {
    return map.get(type);
  }


}
