package simplefactory;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class CheesePizza extends Pizza {
  public CheesePizza() {
    name = "Simple Cheese pizza";
    dough = "Thin crust";

    toppings.add("Cheese");
  }
}
