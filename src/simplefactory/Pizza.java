package simplefactory;

import java.util.ArrayList;
import java.util.List;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public abstract class Pizza {

  public String name;
  public String dough;
  public List<String> toppings = new ArrayList<String>();

  public void prepare() {
    stdout("Preparing pizza " + name);
  }

  public void cut() {
    stdout("Cutting pizza");
  }

  public void bake() {
    stdout("Baking pizza");
  }

   public void box() {
    stdout("Boxing pizza");
  }

  /**
   * User: gsunderam
   * Date: May 22, 2011
   */
  public static class TestSimplePizza {

    public static void main(String[] args) throws Exception {
      SimplePizzaFactory factory = new SimplePizzaFactory();
      PizzaStore store = new PizzaStore(factory);

      Pizza pizza = store.orderPizza("VEGGIE");

      stdout("Pizza ordered is " + pizza.name);
    }
  }
}
