package simplefactory;

/**
 * User: gsunderam
 * Date: May 22, 2011
 */
public class PizzaStore {
  SimplePizzaFactory factory;

  public PizzaStore(SimplePizzaFactory factory) {
    this.factory = factory;
  }

  public Pizza orderPizza(String type) throws Exception {
    Pizza pizza = factory.createPizza(type);

    if (pizza != null) {
      pizza.prepare();
      pizza.bake();
      pizza.cut();
      pizza.box();
    }

    return pizza;
  }

}
