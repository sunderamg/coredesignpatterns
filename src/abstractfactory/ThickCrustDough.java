package abstractfactory;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public class ThickCrustDough implements Dough {

  public String getName() {
    return "Thick Crust";
  }
}
