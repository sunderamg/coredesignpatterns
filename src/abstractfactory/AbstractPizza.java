package abstractfactory;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public abstract class AbstractPizza {

  Dough dough;  //These are families of products
  Cheese cheese;
  Sauce sauce;
  String [] veggies;
  Clams clam;
  String name;

  //This method is the crux of the action.
  public abstract void prepare();

  public void cut() {
    stdout("Cutting pizza");
  }

  public void bake() {
    stdout("Baking pizza");
  }

   public void box() {
    stdout("Boxing pizza");
  }
}
