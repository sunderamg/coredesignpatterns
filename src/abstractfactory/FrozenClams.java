package abstractfactory;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public class FrozenClams implements Clams {

  public String getName() {
    return "Frozen Clams";
  }
}
