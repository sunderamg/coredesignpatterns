package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public class CreamCheese implements Cheese {

  String name = "CreamCheese";

   public String getName() {
     return name;
   }
}
