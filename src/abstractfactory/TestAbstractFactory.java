package abstractfactory;

import factory.CAPizzaStore;
import factory.NYStylePizzaStore;
import factory.NewPizzaStore;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Oct 19, 2010
 * Test harness to exercise the abstract factory pattern.
 */
public class TestAbstractFactory {
    public static void main(String [] args) {
        stdout("--------------------Output begins------------------------");
        AbstractPizzaStore abstractStore = new NYPizzaStore();
        AbstractPizza pizza = abstractStore.orderPizza("cheese");
        stdout("Succes Got NY Pizza: " + pizza.name);
        stdout("------------------------------------------------");

        AbstractPizzaStore caPizzaStore = new CAStore();
        AbstractPizza caPizza = caPizzaStore.orderPizza("cheese");
        stdout("Success Got California " + caPizza.name + " pizza");
    }
}
