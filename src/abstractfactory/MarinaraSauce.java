package abstractfactory;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public class MarinaraSauce implements Sauce {
  public String getName() {
    return "Marinara";
  }
}
