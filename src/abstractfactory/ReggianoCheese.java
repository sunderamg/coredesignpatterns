package abstractfactory;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public class ReggianoCheese implements Cheese {

  public String getName() {
    return "Reggiano";
  }
}
