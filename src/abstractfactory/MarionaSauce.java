package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public class MarionaSauce implements Sauce {
  String name = "Mariona Sauce";

  public String getName() {
    return name;
  }
}
