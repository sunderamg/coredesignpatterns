package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public class ThinDough implements Dough {

  String name = "Thin Dough";

  public String getName() {
    return name;
  }
}
