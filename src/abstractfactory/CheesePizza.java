package abstractfactory;

import java.util.Arrays;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 24, 2011
 * 
 * This is the client for abstract factory. Only the main factory interface, two factory impls and this class
 * is relevant in the context of ABSTRACT FACTORY pattern. The code is intermixed with Factory Method pattern
 * just like that.
 */
public class CheesePizza extends AbstractPizza {
  private IngredientFactory ingredientFactory;
  
  public CheesePizza(IngredientFactory ingredientFactory) {
    this.name = "Cheese";
    this.ingredientFactory = ingredientFactory;
  }

  /**
   * Here the ingredientFactory creates a family of related products and returns them as an interface
   * that is common to ALL sauce, dough or cheese types. This is ABSTRACT FACTORY IN ACTION, where as out
   * ingredientFactory is THE ABSTRACT FACTORY. This class is the CLIENT for abstract factory
   */
  @Override
  public void prepare() {
     stdout("Preparing " + ingredientFactory.getName() + " " + name);
     //All these are families of products produced by the abstract factory
     sauce = ingredientFactory.createSauce();
     dough = ingredientFactory.createDough();
     cheese = ingredientFactory.createCheese();
     veggies = ingredientFactory.createVeggies();

		 //call simple methods without actually knowing what type of Sauce was returned
     stdout("Sauce: " + sauce.getName() + " dough: " + dough.getName() + " cheese: " + cheese.getName());
     stdout("Veggies: " + Arrays.toString(veggies));
  }
}
