package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 * This is the actual Abstract Factory pattern. Sauce, Dough are all abstract classes
 */
public interface IngredientFactory {
  public Sauce createSauce();
  public Dough createDough();
  public Cheese createCheese();
  public String [] createVeggies();
  public Clams createClams();
  public String getName();
}
