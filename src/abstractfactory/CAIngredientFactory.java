package abstractfactory;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public class CAIngredientFactory implements IngredientFactory {

  String regionalArea = "California";
  
  public Sauce createSauce() {
    return new MarinaraSauce();
  }

  public Dough createDough() {
    return new ThickCrustDough();
  }

  public Cheese createCheese() {
    return new ReggianoCheese();
  }

  public String[] createVeggies() {
    return new String[] {"Cabbage", "Tomatoes"};
  }

  public Clams createClams() {
    return new FrozenClams();
  }

  public String getName() {
    return this.regionalArea; 
  }
}
