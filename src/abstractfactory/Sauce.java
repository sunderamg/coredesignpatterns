package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public interface Sauce {

  public String getName();
}
