package abstractfactory;

import simplefactory.PizzaType;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public class CAStore extends AbstractPizzaStore {
  PizzaType pizzaType;
  IngredientFactory ingredientFactory = new CAIngredientFactory();

  /**
   * This method IS the FACTORY METHOD. Based on type it creates various pizzas
   *
   * @param type
   * @return
   */
  @Override
  protected AbstractPizza createPizza(String type) {
     AbstractPizza pizza = null;

    if (PizzaType.getEnumByType(type) == pizzaType.CHEESE) {
      pizza = new CheesePizza(ingredientFactory);
    } //other else conditions here

    return pizza;
  }
}
