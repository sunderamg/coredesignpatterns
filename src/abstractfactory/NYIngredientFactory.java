package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public class NYIngredientFactory implements IngredientFactory {

  public String regionalArea = "NY";

  public Sauce createSauce() {
    return new MarionaSauce();   
  }

  public Dough createDough() {
    return new ThinDough();
  }

  public Cheese createCheese() {
    return new CreamCheese();
  }

  public String[] createVeggies() {
    return new String[] {"onion", "cabbage", "avacado", "lettuce"};
  }

  public Clams createClams() {
    return new FreshClams();
  }

  public String getName() {
    return this.regionalArea;
  }
}
