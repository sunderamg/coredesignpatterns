package abstractfactory;

/**
 * User: gsunderam
 * Date: May 24, 2011
 */
public interface Cheese {
   public String getName();
}
