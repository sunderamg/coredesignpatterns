package abstractfactory;

/**
 * User: gsunderam
 * Date: May 25, 2011
 */
public abstract class AbstractPizzaStore {

  public AbstractPizza orderPizza(String type) {
    //This is the factory method pattern
    AbstractPizza pizza = createPizza(type);

    //create pizza will refer to the actual instance of the Pizza i.e. cheese etc.
    pizza.prepare(); //This delegates to the corresponding prepare method of CheesePizza or VeggiePizza
    pizza.bake();
    pizza.cut();
    pizza.box();

    return pizza;
  }

   protected abstract AbstractPizza createPizza(String type);
}
