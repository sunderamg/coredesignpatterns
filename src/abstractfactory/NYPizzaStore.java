package abstractfactory;

import factory.NewPizzaStore;
import simplefactory.PizzaType;

/**
 * User: gsunderam
 * Date: May 25, 2011
 * Now the Regional stores such as NY accept an ingredient factory from which they can make
 * custom preparation using specific minerals
 */
public class NYPizzaStore extends AbstractPizzaStore {

  PizzaType pizzaType;
  IngredientFactory ingredientFactory = new NYIngredientFactory();

  /**
   * This method IS the FACTORY METHOD. Based on type it creates various pizzas
   * 
   * @param type
   * @return
   */
  @Override
  protected AbstractPizza createPizza(String type) {
     AbstractPizza pizza = null;

    if (PizzaType.getEnumByType(type) == pizzaType.CHEESE) {
      pizza = new CheesePizza(ingredientFactory);
    } //other else conditions here

    return pizza;
  }
}

