package corejava;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 2, 2010
 * Time: 5:10:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShortTypeEx {
    public static void main(String[] args) {
        Short s = 32767;
        Integer i = 32767;
        if (i.equals(s)) stdout("TRUE");
        else stdout("FALSE");
        if (i.intValue() == s.shortValue()) stdout("CAN compare primitive shorts and ints");
        stdout("Short MAX value: " + Short.MAX_VALUE + " MIN: " + Short.MIN_VALUE);
        stdout("Integer MAX value: " + Integer.MAX_VALUE + " MIN: " + Integer.MIN_VALUE);
        Short s1 = (short) 343 / (short) 49;
        stdout(s1);
    }
}
