package corejava;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 5, 2010
 * Time: 10:20:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class WrapperEx {
    static Double d;

    public static void main(String[] args) {
        String s1 = "Success";
        String s2 = "Success";
        stdout("s1 == s2 ? " + (s1 == s2));   //true because strings are different from other objects
        Integer i1 = 1000; //in java parlance in that strings have a string constant pool that JVM could
        Integer i2 = 1000; //use
        stdout(i1 == i2 ? "same object" : "Different objects");
        stdout(i1.equals(i2));  //this behavior is NOT the same as Strings as seen in the previous output
        Integer lowInt1 = -128; //the below will be false for ints outside the short range -128 thro 127
        Integer lowInt2 = -128;
        stdout(lowInt1 == lowInt2 ? "same object for short range values" : "Different objects");
        doStuff(d);
        //WidenAndBox
        byte b = 5;
        go(b); //byte is converted to Byte and then upcast to object!
        wideVarArg(5,5);
        boxVarArg(5,5);
        int x = 1000;
        if (i1.equals(x)) { stdout("equals true for I and i"); } //primitive comparison
    }

    private static void boxVarArg(Integer i, Integer i1) {
        stdout("Auto boxed var-arg ints to Integer wrappers");
    }

    private static void wideVarArg(long... l) {
        stdout("widened var-arg ints to long");
    }

    private static void go(Object byteObj) {
        Byte aByte = (Byte) byteObj;
        stdout("Long called : " + aByte);
    }

    private static void doStuff(Double d) {
//        stdout("Double value " + d++); //CAN THROW NPEF
    }
}
