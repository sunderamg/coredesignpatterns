package corejava;

import java.text.MessageFormat;
import java.util.*;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Aug 16, 2011
 *
 * Message formatting based on locales n java.text package
 */
public class JavaTextExample {
  public static void main(String[] args) {
    int fileCount = 23;
    String diskName = "fic1";

    Object[] testArgs = {fileCount, diskName};
    MessageFormat form = new MessageFormat("The disk \"{1}\" contains {0} file(s).");

    stdout(form.format(testArgs));

    //Get the file encoding
    stdout("Encoding for JDBC Drivers, File I/O: " + System.getProperty("file.encoding"));
    System.setProperty("file.encoding", "UTF-8");

    //Set UTF-8 as the new encoding
    stdout("New Encoding: " + System.getProperty("file.encoding"));

    //Print out the System properties
    Properties p = System.getProperties();
    Set<Map.Entry<Object,Object>> entries = p.entrySet();
    Iterator<Map.Entry<Object, Object>> it = entries.iterator();

    while(it.hasNext()) {
      Map.Entry<Object, Object> keyValue = it.next();
      stdout("Name: " + keyValue.getKey() + " Value: " + keyValue.getValue());
    }

    stdout(UUID.randomUUID());
    stdout(UUID.fromString("GS"));
  }
}
