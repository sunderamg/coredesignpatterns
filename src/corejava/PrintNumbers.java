package corejava;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Nov 11, 2010
 * Time: 9:19:14 PM
 * This was asked at McKesson
 */
public class PrintNumbers {

    static int size = 7;

    static void changeIt(int size) {
        size += 200;
        stdout("size inside changeIt = " + size);
    }

    public static void main(String [] args) {
        Object [] obj = new Object[3];  //All will be set to null. if we don't construct the array, NPE at line stdout(o)
        obj[0] = "GS is God";
        stdout("Uninitialized objects");
        for (Object o : obj) stdout(o);
        //int [] ia = null; //BEWARE: Causes the infamous NPE!
        int [] ia = new int[3]; //produces 0's
        stdout("Uninitialized int's");
        for (int i : ia) stdout(i);
        int [] ina = new int[4];
        ia = ina; //valid to assign array ref to another int [] of any dimension
        byte b = 5; //ok as each of the types below CAN be widened to an int
        ina[0] = new Integer("15"); //auto unboxed to a primitive.
        ina[3] = b;
        ina[2] = 'f';
        ina[1] = (short) 7;
        stdout("--------Initialized int's-------");
        for (int i : ina) stdout(i);  //cool.
        byte [] ba = new byte [4];
        //ina = ba; //No! can't assign byte [] to int []
        Integer [] in = new Integer[4];
        //ia = in; //can't assign a wrapper array to the primitive
        //printEvenOdds();
        int x;
        if (args.length == 6) {
            x = 6;  //ok to do this without initializing.we can READ x,
                    // but not write before initializing local variables
            x = ia[0]; //ok to READ x.
            x = ia[0] + ia[2]; //ok to READ x
        }
        //int y = x; //Won't compile as x is uninitialized and WRITE to y
        //stdout(x); //Won't compile as we're trying to WRITE it
        PrintNumbers pn = new PrintNumbers();
        stdout("size before changeIt =" + size);
        changeIt(size);
        stdout("size after changeIt = " + size);

    }

    private static void printEvenOdds() {
        for(int i = 1; i<= 50; i++) {
            if (i % 5 == 0) stdout("Fizz buzz");
            else if (i % 2 == 0) stdout(i + "-Fizz");
            else if (i % 2 != 0) stdout(i + ":Buzz");
        }
    }
}
