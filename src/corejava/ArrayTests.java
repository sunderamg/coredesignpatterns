package corejava;

import log.Logger;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Jan 27, 2011
 * Time: 7:51:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class ArrayTests {

    static Car [] c; //Will be null by default
    public static void main(String[] args) {
        Car [] cars = null; //need explicit initialization
        stdout(c + " " ); //null
        cars = new Car[3]; //All elements will be made to null
        //cars[0] = new Car();
        //cars[1] = new Car();
        //cars[2] = new Car();
        for (Car car : cars) {
            stdout(car);
        }
        stdout("Subtype assignments"); //can assign a subtype to a super type within an array.
        Automobile [] types = new Automobile[] {cars[0], new Automobile(), new Automobile(), cars[2]};
        for (Automobile auto : types) {
            stdout(auto);
        }
        stdout("----------------primitive assignments-----------------");
        int a [] = new int[4];
        a[0] = 'a';   //All these are legal as these types fit into 32 bit int
        a[1] = (byte) 120; //widening conversion cool!
        a[2] = (short) 12;
        a[3] = (int) 234567L; //cast required. narrowing conversion
        for (int i : a) {
            stdout(i);
        }
        stdout("-------printing two dimensional arrays----------");
        int [][] twoDim = {{23, 34, 34}, {10, 345, 456}}; //new int [2][3]; declare / declare and initialize
        int [][] anotherTwoDim = twoDim;
        for (int [] i : anotherTwoDim) {
            for (int j : i) {
                stdout(j);
            }
        }
        
    }

}

class Automobile {
    String type = "car";
}

class Car extends Automobile {
    String make = "honda";
}
