package corejava;

import log.Logger;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 12, 2010
 * Time: 11:13:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoopExamples {
    public static void main(String[] args) {
        int age = 0;
        outer:
            while(age <= 21) {
                age++;
                if (age == 16) {
                    stdout("Get the license");
                    continue;
                }
                stdout("Another year");
            }
    }
}
