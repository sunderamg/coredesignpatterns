package corejava;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 8, 2010
 * Time: 10:44:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class Hawk extends Raptor {
    public static void main(String[] args) {
        stdout("pre");
        new Hawk();
        stdout("Hawk");
    }
}

class Raptor extends Bird {
    static { stdout("r1");}
    public Raptor() {
        stdout("r2");
    }
    {
        stdout("r3");}
    static { stdout("r4");}

}

class Bird {
    static { stdout("b1");}
    { stdout("b2");}

    public Bird() {
       stdout("b3");
    }
}
