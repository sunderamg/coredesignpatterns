package corejava;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 9, 2010
 * Time: 10:43:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class OperatorTest {
    public static void main(String[] args) {
        boolean b = false;
        //Condition must evaluate to boolean for the code to compile
        if (b = true) {
            stdout("b is true");
        } else {
            stdout("b is false");
        }
        byte x = 1;
        stdout("x is " + (x=2));
        int y = 1;
        y *= 2 + 5; //y = y * (2 + 5)
        stdout("y = " + y);
        String s = "54";
        s += 23;
        s += 23;
        stdout("String s = " + s);
        int z = 7;
        if ( ++z > 5 | ++z > 6) {   //Short circuit OR operator
            z++;
        }
        stdout("z after Non short circuit operator = " + z);
        z = 7;
        if ( ++z > 5 || ++z > 6) {  //non Short circuit OR operator
            z++;
        }
        stdout("z after short circuit operator = " + z);
        stdout(1 + 2 + "foo");
        char c = 'f';
        switch (c) { //legal to use any int, char, byte, short as all of these fit into 4 bytes
            default: stdout("Default"); //LEGAL! DEFAULT can be anywhere in the case stack
            case 'f': stdout("Implicit widening conversion");
                     break;        //byte x promoted to int
            case 0 : stdout("ZERO is allowed");
                     break;          //same as case 23 above
            //default: stdout("Default");  -- legaL
            // case 128: Not legal as 128 cannot fit into a 8-bit BYTE 
        }
    }
}
