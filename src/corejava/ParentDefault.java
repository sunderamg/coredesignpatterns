package corejava;

import static log.Logger.stdout;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Jan 15, 2011
 * Time: 5:14:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParentDefault {
    protected int testVar = 5;

    public ParentDefault(String s) {
        stdout("Inside parent constructor in a different package " + s);
    }
}
