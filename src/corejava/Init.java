package corejava;

import static log.Logger.log;

/**
 * User: gsunderam
 * Date: Dec 5, 2010
 * Initialization examples. static blocks run FIRST in the ORDER in which they are decalred in the source
 * code, followed by instance blocks in the ORDER they appear in the source and finally the constructors
 */
public class Init {
    Init() { log("no-arg-constructor");}
    Init(int n) { log("1-arg-constructor");}
    static { log("First static init"); }
    { log("First instance init");}
    { log("second instance init");}
    static { log("second static init"); }

    public static void main(String[] args) {
        new Init();
        new Init(7);
    }
}
