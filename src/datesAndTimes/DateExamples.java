package datesAndTimes;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static log.Logger.stdout;

/**
 * Demonstrates just the classic use cases of DF class with locale information
 */
public class DateExamples {
    public static void main(String [] args) {
        Date d1 = new Date(109000000000L);
        stdout("Date object: " + d1);
        DateFormat df = DateFormat.getDateInstance();
        String dateString = df.format(d1);
        DateFormat defaultformat = DateFormat.getInstance();
        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance();
        stdout("DateInstance: " + dateString);
        stdout("Instance: " + defaultformat.format(d1));
        stdout("DateTime: " + dateTimeInstance.format(d1));
        stdout("---Printing different styles---");
        stdout("Short: " + DateFormat.getDateInstance(DateFormat.SHORT).format(d1));
        stdout("Medium: " + DateFormat.getDateInstance(DateFormat.MEDIUM).format(d1));
        stdout("long: " + DateFormat.getDateInstance(DateFormat.LONG).format(d1));
        stdout("full: " + DateFormat.getDateInstance(DateFormat.FULL).format(d1));
        Calendar cal = Calendar.getInstance();
        cal.setTime(d1);
        cal.roll(Calendar.MONTH, 12);
        stdout("Rolled month date: " + cal.getTime()); //Roll doesn't increment year, month and date
        cal.add(Calendar.MONTH, 12);
        stdout("Added month date: " + cal.getTime()); //Add does increment the above fields
        try {
            stdout("Parse the date " + df.parse(dateString)); //Some precision on time will be lost
            String style = "mm/dd/yy";

        } catch (ParseException e) { e.printStackTrace(); }
    }
}
