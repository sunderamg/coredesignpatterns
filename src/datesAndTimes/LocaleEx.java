package datesAndTimes;

import log.Logger;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;

import static log.Logger.log;

/**
 * Created by IntelliJ IDEA.
 * User: gsunderam
 * Date: Dec 1, 2010
 * Time: 2:40:22 PM
 * Locale examples
 */
public class LocaleEx {
    public static void main(String [] args) {
        Locale locCA = new Locale("en", "ca");
        Locale locIt = new Locale("it", "IT");
        Locale locDK = new Locale("da", "DK");
        Calendar cal = Calendar.getInstance();
        cal.set(2010, 12, 06);
        DateFormat dfIt = DateFormat.getDateInstance(DateFormat.FULL, locIt);  //Specify locale at construction time ONLY
        Logger.stdout("Time in italian locale is " + "'" + dfIt.format(cal.getTime()) + "'");
        Logger.stdout("default country " + locIt.getDisplayCountry());
        Logger.stdout("default language " + locIt.getDisplayLanguage());
        Logger.stdout("In denmark italian language is " + locIt.getDisplayLanguage(locDK ));
        Logger.stdout("Italy is known as " + locIt.getDisplayCountry(locIt) + " in Italy");
        Logger.stdout("In italy danish is known as " + locDK.getDisplayLanguage(locIt));
        Logger.stdout("In canada english is known as " + locCA.getDisplayLanguage(locCA));
        Logger.stdout("In canada, country name is " + locCA.getDisplayCountry(locCA));
        Logger.stdout("In italy, Canada is spelt as " + locCA.getDisplayCountry(locIt));
        Logger.stdout("In denmark Italy is spelt as " + locIt.getDisplayCountry(locDK));
        //Number format examples
        NumberFormat [] nf = new NumberFormat[5];
        nf[0] = NumberFormat.getInstance();
        nf[1] = NumberFormat.getNumberInstance();
        nf[1].setMinimumFractionDigits(2);//change these two setting and review the output. its fun to note the nuances
        nf[1].setMaximumFractionDigits(2);
        nf[2] = NumberFormat.getInstance(locCA);
        nf[3] = NumberFormat.getCurrencyInstance();
        nf[4] = NumberFormat.getCurrencyInstance(locCA);
        float f1 = 349.9035f;
        Logger.stdout("-----------Number format examples-------");
        for (NumberFormat nfa : nf) {
            Logger.stdout(nfa.format(f1)); //rounds of to 349.904!
        }
        nf[0].setParseIntegerOnly(true);
        try {
            Logger.stdout("Parsed string: " + nf[0].parse(String.valueOf(f1)));
        } catch (ParseException e) {
            e.printStackTrace();  
        }
    }
}
