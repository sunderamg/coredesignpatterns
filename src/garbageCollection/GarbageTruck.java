package garbageCollection;

import java.util.Date;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: Dec 7, 2010
 */
public class GarbageTruck {
    private static int gcCount = 0;

    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        stdout("Total JVM Memory = " + rt.totalMemory());
        stdout("Free memory available = " + rt.freeMemory());
        Date d = null;
        for (short i = 0;i <= 10000;i++) {
           d = new Date();
           d = null;
        }
        long fm = rt.freeMemory();
        stdout("After free memory = " + fm);
        System.gc(); //recommended over rt.gc()
        long gcm = rt.freeMemory();
        stdout("After GC Memory = " + gcm);
        stdout("Memory freed by the gc call: " + (gcm - fm));
        new GarbageTruck(); //This will become eligible for GC after this line!, so the JVM calls the finalize
        System.gc(); //Check whether finalize is called
    }

    /**
     * Will be called by the JVM once per instance of this class before the instance is deleted from the heap
     */
    protected void finalize() {
        gcCount++;
        stdout("Called finalize by the JVM " + gcCount);
    }
}
