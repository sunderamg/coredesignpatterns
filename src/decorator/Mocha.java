package decorator;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 20, 2011
 * Decorator class
 */
public class Mocha extends CondimentDecorator {

  private Beverage beverage;

  public Mocha(Beverage beverage) {
    this.beverage = beverage;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " Mocha";
  }

  @Override
  public int getSize() {
    return beverage.getSize();  
  }

  @Override
  public double cost() {
    stdout("Adding cost of MOCHA");
		double cost = beverage.cost();
		stdout("Post: Added cost of Mocha");
		return cost + 1.99;
  }
}
