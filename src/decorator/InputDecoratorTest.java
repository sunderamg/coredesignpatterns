package decorator;

import java.io.*;

import static log.Logger.print;
import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 21, 2011
 * Decorator example with Java I/O
 */
public class InputDecoratorTest {

  public static void main(String[] args) {
    int c;

    try {
      InputStream in = new FileInputStream("temp.txt"); //Component.
      in = new BufferedInputStream(in);   //decorator 1 === Mocha
      in = new UpperCaseInputStream(in); //decorator 2 === whip

      while ((c = in.read()) >= 0) {
        print((char)c);
      }
    } catch (IOException e) {
      stdout(e.getMessage());
    }

  }
}
