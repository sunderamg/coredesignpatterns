package decorator;

/**
 * User: gsunderam
 * Date: May 19, 2011
 */
public class HouseBlend extends Beverage {

  public HouseBlend(int size) {
    this.size = size;
  }

  public HouseBlend() {
    this.size = CoffeeSize.TALL.getSize();
  }

  @Override
  public String getDescription() {
    return "HouseBlend";
  }

  @Override
  public double cost() {
    return 33;
  }
}
