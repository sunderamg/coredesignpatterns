package decorator;

/**
 * User: gsunderam
 * Date: May 20, 2011
 */
public enum CoffeeSize {

  TALL(1), GRANDE(2), VENTI(3);

  private int size;

  CoffeeSize(int i) {
    this.size = i;
  }

  public int getSize() {
     return size;
  }
}
