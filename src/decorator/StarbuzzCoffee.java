package decorator;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 20, 2011
 * Class to exercise the Decorator pattern
 */
public class StarbuzzCoffee {

  public static void main(String[] args) {
    Beverage houseBlend = new HouseBlend();
    stdout("Beverage: " + houseBlend.getDescription());

    houseBlend = new Mocha(houseBlend); //decorate with mocha, soy and whip
    houseBlend = new Soy(houseBlend); //This is why we had to have the condiment and the beverage be of the same type
    houseBlend = new Whip(houseBlend);
    
    //Then find the real description and cost! cost delegated continually to its peer condiments and finally to
    //the actual beverage, houseblend in this case
    stdout("Descrition: " + houseBlend.getDescription() + " Cost: " + houseBlend.cost());
  }
}
