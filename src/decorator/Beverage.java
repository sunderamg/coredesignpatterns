package decorator;

/**
 * User: gsunderam
 * Date: May 19, 2011
 * This is the parent of all condiments and beverages.
 */
public abstract class Beverage {

  protected int size;

  public String getDescription() {
    return "Beverage";
  }

  public int getSize() {
     return size;
  }

  public void setSize(int size) {
    this.size = size;
  };

  public abstract double cost();
}
