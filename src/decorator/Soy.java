package decorator;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 20, 2011
 */
public class Soy extends CondimentDecorator {

  private Beverage beverage;

  public Soy(Beverage beverage) {
    this.beverage = beverage;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " Soy";
  }

  @Override
  public int getSize() {
    return beverage.getSize();
  }

  @Override
  public double cost() {
    stdout("Pre: Adding cost of SOY");
		double cost = beverage.cost();
		stdout("Post: Added cost of Soy");
		return cost + 2;
  }
}
