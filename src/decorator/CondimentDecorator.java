package decorator;

import decorator.Beverage;

/**
 * User: gsunderam
 * Date: May 19, 2011
 * This is the base abstract decorator class that every decorator must extend
 */
public abstract class CondimentDecorator extends Beverage {

  public abstract String getDescription();
}
