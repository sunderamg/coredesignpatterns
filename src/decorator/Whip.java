package decorator;

import static log.Logger.stdout;

/**
 * User: gsunderam
 * Date: May 20, 2011
 */
public class Whip extends CondimentDecorator {

  private Beverage beverage;

  public Whip(Beverage beverage) {
    this.beverage = beverage;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " Whip";
  }

  @Override
  public int getSize() {
    return beverage.getSize();
  }

  @Override
  public double cost() {
    stdout("PreProcessing: Adding cost of WHIP");
		double cost = beverage.cost();
		stdout("Post Processing: Added cost of whip");
		return cost + 2.4;
  }

}
