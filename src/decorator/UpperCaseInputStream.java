package decorator;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * User: gsunderam
 * Date: May 21, 2011
 * Decorator using I/O. This decorator converts lowercase to uppercase. It decorates the FileInputStream.
 * Component is FIS Decorators are BIS and this one
 */
public class UpperCaseInputStream extends FilterInputStream {

  /**
   * Creates a <code>FilterInputStream</code>
   * by assigning the  argument <code>in</code>
   * to the field <code>this.in</code> so as
   * to remember it for later use.
   *
   * @param in the underlying input stream, or <code>null</code> if
   *        this instance is to be created without an underlying stream.
   */
  protected UpperCaseInputStream(InputStream in) {
    super(in);
  }

  /**
   * read using a byte array. This method delegates to the BIS and then to FIS as was the case in the
   * Starbuzzcoffee design. first to mocha, soy and then FINALLY the HouseBlend "Component"
   *
   * @param b
   * @param offset
   * @param len
   * @return
   * @throws IOException
   */
  public int read(byte [] b, int offset, int len) throws IOException {
    int c = super.read(b, offset, len);
    for (int i = offset;i < offset + len;i++) {
      b[i] = (byte) Character.toUpperCase((char)b[i]);
    }
    return c;
  }

  public int read() {
    int c = 0;
    try {
      c = super.read();
    } catch (IOException e) {
      e.printStackTrace(); 
    }
    return (c == -1 ? c : Character.toUpperCase((char)c));
  }
}
